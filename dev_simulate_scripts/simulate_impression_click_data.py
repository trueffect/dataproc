# ------------------------------------------------------------------------------
#
# Simulates user Impressions and Clicks by firing the tag directly against
#   the ad server.  Use crontab to run every 10 minutes so that there is
#   test data generated in each of the ad server's cookie logs.
#
# ------------------------------------------------------------------------------

import requests


# impression url and number of times to fire each time the script runs
impression_url = [
    'http://extdev.adlegend.net/iframe?spacedesc=9569660_9126942_100x56_9126943_9569660&target=_blank&random=&@CPSC@=',
    'http://ext.adlegend.net/iframe?spacedesc=6602509_6164058_100x100_6164059_6602509&target=_blank&random=&@CPSC@=',
    'http://ext.adlegend.net/iframe?spacedesc=6602512_6016102_100x200_6467256_6602512&target=_blank&random=&@CPSC@=',
    'http://ext.adlegend.net/iframe?spacedesc=6602515_6016084_100x300_6049237_6602515&target=_blank&random=&@CPSC@='
]
impression_count = 5

# click url and number of times to fire each time the script runs
click_url = [
    'http://extdev.adlegend.net/click.ng?spacedesc=9569660_9126942_100x56_9126943_9569660&af=9569663&ml_pkgkw=-%253A%2522%2522&ml_pbi=-9569660&ml_camp=9181618&ml_crid=9569664&click=http://www.speedsters.com/',
    'http://ext.adlegend.net/click.ng?spacedesc=6602509_6164058_100x100_6164059_6602509&af=35030&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6602509&ml_camp=6602507&ml_crid=6602518&click=http://www.trueffect.com',
    'http://ext.adlegend.net/click.ng?spacedesc=6602512_6016102_100x200_6467256_6602512&af=35030&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6602512&ml_camp=6602507&ml_crid=6602519&click=http://www.trueffect.com',
    'http://ext.adlegend.net/click.ng?spacedesc=6602515_6016084_100x300_6049237_6602515&af=35030&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6602515&ml_camp=6602507&ml_crid=6602520&click=http://www.trueffect.com'
]
click_count = 1

# build header data
ip_address = '208.186.79.94'    # whitelisted ip address
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36'
headers = {'User-Agent': user_agent, 'x-forwarded-for': ip_address}

# build cookie data (for associated data)
bid = '555-1212'
cookies = {'PrefID': bid}

# fire impressions
for i in range(0, impression_count):
    for i_url in impression_url:
        print('firing impression: ' + i_url)
        response = requests.get(i_url, headers=headers, cookies=cookies)
        print(response.status_code)

# fire clicks
for i in range(0, click_count):
    for c_url in click_url:
        print('firing click: ' + c_url)
        response = requests.get(c_url, headers=headers, cookies=cookies)
        print(response.status_code)

# ------------------------------------------------------------------------------
