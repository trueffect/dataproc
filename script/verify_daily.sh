#!/usr/bin/env bash

# success email
success_email="qa@trueffect.com"

# failure email
fail_email="qa@trueffect.com,tellebracht@trueffect.com"

# qa server's hostname
qaserver="mercury.trueffect.com"

# make the script portable so it runs on both mercury and the localhost
echo "Hostname: $HOSTNAME"
if [ $HOSTNAME = "$qaserver" ]; then
    echo "Hi $qaserver!"
    dy=`date -u -d "now -1 days" +%Y%m%d`
    export ORACLE_HOME=/app/oracle/11g
    export LD_LIBRARY_PATH=/app/oracle/11g/lib
    pyexe="/usr/local/python/bin/python3"
    pypath="/home/prodman/simulateTraffic/end_to_end/dataproc/script/"
else
    echo "Not $qaserver"
    dy=`date -u -v -24H +%Y%m%d`
    fail_email="wbutler@trueffect.com"
    pyexe="/usr/local/bin/python3"
    pypath=""
fi
fail_email="wbutler@trueffect.com"
echo "Day to Verify: $dy"

# prepare and execute the python script
cmd=$pyexe" "$pypath"verify_driver_daily.py"
echo "cmd: $cmd"
output=`${cmd}`
ret=$?
echo "Result: {$output}"

# send email for result
echo $ret
if [ $ret -ne 0 ]; then
    #Fail
    echo "Sending FAIL email..."
    echo -e "day_id: $dy, output: $output" | mail -s "QA Creative Versioning FAILED for day $dy" "$fail_email"
else
    #Pass
    echo "Sending PASS email..."
    echo -e "Prodman is happy for day $dy :)" | mail -s "QA Creative Versioning PASSED for day $dy"  "$success_email"
fi
echo " - done sending email"
