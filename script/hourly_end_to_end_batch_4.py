# ------------------------------------------------------------------------------
#
# Batch 2 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import batch

# ------------------------------------------------------------------------------
def run_batch():
    # I, version, version, CP
    batch.play_tag('I', '23')
    batch.play_tag('C', '24')
    batch.play_tag('P', '24')

    # IC, version, version, P
    batch.play_tag('I', '26')
    batch.play_tag('C', '26')
    batch.play_tag('P', '27')

    # ----------------------------------------------------------
    # associated I,P transactions (VT)
    # ----------------------------------------------------------

    # IP same version
    batch.play_tag('I', '29')
    batch.play_tag('P', '29')

    # I, version, P
    batch.play_tag('P', '31')
    batch.play_tag('I', '32')

    # I, version, version, P
    batch.play_tag('I', '35')
    batch.play_tag('P', '36')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
