# ------------------------------------------------------------------------------
#
# Batch 1 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import lib
import batch

# ------------------------------------------------------------------------------
def run_batch():
    # increment the creative version
    lib.inc_creative_version('5982897', '5982878', 'rat-beetle_259x195.jpg', '5982894')
    batch.inc_version()

    # ----------------------------------------------------------
    # unassociated I,C,P
    # ----------------------------------------------------------

    batch.play_tag('I', '01')
    batch.play_tag('C', '02')
    batch.play_tag('P', '03')

    # ----------------------------------------------------------
    # associated I,C,P transactions (CT)
    # ----------------------------------------------------------

    # ICP same version
    batch.play_tag('I', '10')
    batch.play_tag('C', '10')
    batch.play_tag('P', '10')

    # I, version, C, version, P
    batch.play_tag('I', '13')
    batch.play_tag('P', '14')
    batch.play_tag('C', '15')

    # I, version, CP
    batch.play_tag('I', '16')
    batch.play_tag('C', '18')
    batch.play_tag('P', '18')

    # IC, version, P
    batch.play_tag('I', '19')
    batch.play_tag('C', '19')
    batch.play_tag('P', '21')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
