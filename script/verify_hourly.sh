#!/usr/bin/env bash

# success email
success_email="qa@trueffect.com"

# failure email
fail_email="qa@trueffect.com,tellebracht@trueffect.com"

# qa server's hostname
qaserver="mercury.trueffect.com"

# make the script portable so it runs on both mercury and the localhost
echo "Hostname: $HOSTNAME"
if [ $HOSTNAME = "$qaserver" ]; then
    echo "Hi $qaserver!"
    hr=`date -u -d "now -2 hours" +%Y%m%d%H`
    export ORACLE_HOME=/app/oracle/11g
    export LD_LIBRARY_PATH=/app/oracle/11g/lib
    pyexe="/usr/local/python/bin/python3"
    pypath="/home/prodman/simulateTraffic/end_to_end/dataproc/script/"
else
    echo "Not $qaserver"
    hr=`date -u -v -2H +%Y%m%d%H`
    fail_email="wbutler@trueffect.com"
    pyexe="/usr/local/bin/python3"
    pypath=""
fi
fail_email="wbutler@trueffect.com"
echo "Hour to Verify: $hr"

# prepare and execute the python script
cmd=$pyexe" "$pypath"verify_driver_hourly.py"
echo "cmd: $cmd"
output=`${cmd}`
ret=$?
echo "Result: {$output}"

# send email for result
echo $ret
if [ $ret -ne 0 ]; then
    #Fail
    echo "Sending FAIL email..."
    echo -e "hr: $hr, output: $output" | mail -s "QA Creative Versioning FAILED for hour $hr" "$fail_email"
else
    #Pass
    echo "Sending PASS email..."
    echo -e "Prodman is happy for hour $hr :)" | mail -s "QA Creative Versioning PASSED for hour $hr" "$success_email"
fi
echo " - done sending email"
