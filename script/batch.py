# ------------------------------------------------------------------------------
#
# library to support simulate scripts for batches
#
# ------------------------------------------------------------------------------
import subprocess
import lib
import requests

# browser_id prefix to use
bid_prefix = '888-1'

# impression, click, ping urls
ver = lib.get_creative_version('5982897')
i_url = 'http://ext.adlegend.net/iframe?spacedesc=5982883_5982857_259x195_5982858_5982883&target=_blank&random=&test=e2e&version=' + str(ver) + '&@CPSC@='
c_url = 'http://ext.adlegend.net/click.ng?spacedesc=5982883_5982857_259x195_5982858_5982883&af=5982894&ml_pkgkw=-%253A%2522%2522&ml_pbi=-5982883&ml_camp=5982878&ml_crid=5982897&test=e2e&version=' + str(ver) + '&click=http://www.thesamba.com/vw/classifieds/cat.php?id=65'
p_url = 'http://ext.adlegend.net/ping?spacedesc=5982924_1061349_1x1_1061349_1061349&db_afcr=123&group=BeetleGroup&event=BeetleEvent&revenue=10&test=e2e&version=' + str(ver)

# common header data
ip_address = '208.186.79.94'    # whitelisted ip address
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36'
headers = {'User-Agent': user_agent, 'x-forwarded-for': ip_address}


# ----------------------------------------------------------
#   Creative Versioning
# ----------------------------------------------------------

# ------------------------------------------------------------------------------
def play_tag(trans_type, bid_suffix):
    # set url
    if trans_type.upper() == 'I':
        # Impression
        url = i_url
    elif trans_type.upper() == 'C':
        # Click
        url = c_url
    elif trans_type.upper() == 'P':
        # Ping
        url = p_url
    else:
        print('Invalid trans_type, must be "I", "C" or "P"!')
        return False

    # make request using cURL
    print('----------------------------------------')
    print('trans_type: ' + trans_type)
    print('browser_id: ' + str(bid_prefix + bid_suffix))
    print('url: ' + url)
    p = subprocess.Popen(["curl", "--silent", "--user-agent", user_agent, "--header", "X-Forwarded-For: " + str(ip_address), "--cookie", "PrefID=" + str(bid_prefix) + str(bid_suffix), url], stdout=subprocess.PIPE)
    p.wait()
    return True

# ------------------------------------------------------------------------------
def inc_version():
    global ver
    global i_url
    global c_url
    global p_url

    # impression, click, ping urls
    ver = lib.get_creative_version('5982897') + 1
    i_url = 'http://ext.adlegend.net/iframe?spacedesc=5982883_5982857_259x195_5982858_5982883&target=_blank&random=&test=e2e&version=' + str(ver) + '&@CPSC@='
    c_url = 'http://ext.adlegend.net/click.ng?spacedesc=5982883_5982857_259x195_5982858_5982883&af=5982894&ml_pkgkw=-%253A%2522%2522&ml_pbi=-5982883&ml_camp=5982878&ml_crid=5982897&test=e2e&version=' + str(ver) + '&click=http://www.thesamba.com/vw/classifieds/cat.php?id=65'
    p_url = 'http://ext.adlegend.net/ping?spacedesc=5982924_1061349_1x1_1061349_1061349&db_afcr=123&group=BeetleGroup&event=BeetleEvent&revenue=10&test=e2e&version=' + str(ver)


# ----------------------------------------------------------
#   Realtime Streaming
# ----------------------------------------------------------

def simulate_rt():
    i_tags = [
        'http://ext.adlegend.net/iframe?spacedesc=6566248_5978531_300x250_5978825_6566248&target=_blank&random=&@CPSC@=',
        'http://ext.adlegend.net/iframe?spacedesc=6566251_5978531_300x250_5978532_6566251&target=_blank&random=&@CPSC@=',
        'http://ext.adlegend.net/iframe?spacedesc=6566284_5982857_300x200_5982858_6566284&target=_blank&random=&@CPSC@=',
        'http://ext.adlegend.net/iframe?spacedesc=6566314_5982857_250x300_5982859_6566314&target=_blank&random=&@CPSC@=',
        'http://ext.adlegend.net/iframe?spacedesc=6566317_5982861_250x300_5982889_6566317&target=_blank&random=&@CPSC@=',
        'http://ext.adlegend.net/iframe?spacedesc=6566351_5975458_728x90_5975459_6566351&target=_blank&random=&@CPSC@='
    ]
    c_tags = [
        'http://ext.adlegend.net/click.ng?spacedesc=6566248_5978531_300x250_5978825_6566248&af=6566254&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566248&ml_camp=6566237&ml_crid=6566257&click=http://www.google.com',
        'http://ext.adlegend.net/click.ng?spacedesc=6566251_5978531_300x250_5978532_6566251&af=6566255&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566251&ml_camp=6566237&ml_crid=6566259&click=http://www.yahoo.com',
        'http://ext.adlegend.net/click.ng?spacedesc=6566284_5982857_300x200_5982858_6566284&af=6566287&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566284&ml_camp=6566238&ml_crid=6566288&click=http://www.TruEffect.com?NewClick=http%3A%2F%2Fext.adlegend.net%2Fclick.ng%3Fspacedesc%3D6566284_5982857_300x200_5982858_6566284%26af%3D6566287%26ml_pkgkw%3D-%25253A%252522%252522%26ml_pbi%3D-6566284%26ml_camp%3D6566238%26ml_crid%3D6566288%26click%3D',
        'http://ext.adlegend.net/click.ng?spacedesc=6566314_5982857_250x300_5982859_6566314&af=6566320&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566314&ml_camp=6566239&ml_crid=6566322&click=http://www.msn.com',
        'http://ext.adlegend.net/click.ng?spacedesc=6566317_5982861_250x300_5982889_6566317&af=6566321&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566317&ml_camp=6566239&ml_crid=6566323&click=http://www.cnn.com',
        'http://ext.adlegend.net/click.ng?spacedesc=6566351_5975458_728x90_5975459_6566351&af=6566354&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6566351&ml_camp=6566240&ml_crid=6566356&click=http://www.amazon.com'
    ]
    p_tags = [
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=2',
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=3',
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=4',
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=5',
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=6',
        'http://ext.adlegend.net/ipixel?spacedesc=6566392_1061349_1x1_1061349_1061349&db_afcr=123&target=_blank&group=RT_Group&event=RT_Event&revenue=7'
    ]

    # fire each tag
    for i in range(0, len(i_tags)):
        simulate_tag(i_tags[i], str(300+i))
        simulate_tag(c_tags[i], str(300+i))
        simulate_tag(p_tags[i], str(300+i))

# ------------------------------------------------------------------------------
def simulate_tag(url, bid_suffix):
    # make request using cURL
    print('----------------------------------------')
    print('Firing tag: ' + url)
    print('browser_id: ' + str(bid_prefix + bid_suffix))
    p = subprocess.Popen(["curl", "--silent", "--user-agent", user_agent, "--header", "X-Forwarded-For: " + str(ip_address), "--cookie", "PrefID=" + str(bid_prefix) + str(bid_suffix), url], stdout=subprocess.PIPE)
    p.wait()
    return True

# ------------------------------------------------------------------------------

#simulate_rt()

# ------------------------------------------------------------------------------
