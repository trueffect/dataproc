#!/usr/bin/env bash

hr=`date -u -v-120M +%Y%m%d%H`
dy=`date -u -v-1440M +%Y%m%d`
echo "two hours ago: $hr"
echo "yesterday: $dy"
