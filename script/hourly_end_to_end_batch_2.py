# ------------------------------------------------------------------------------
#
# Batch 1 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import batch

# ------------------------------------------------------------------------------
def run_batch():
    # I, version, version, CP
    batch.play_tag('I', '22')
    batch.play_tag('C', '23')
    batch.play_tag('P', '23')

    # IC, version, version, P
    batch.play_tag('I', '25')
    batch.play_tag('C', '25')
    batch.play_tag('P', '26')

    # ----------------------------------------------------------
    # associated I,P transactions (VT)
    # ----------------------------------------------------------

    # IP same version
    batch.play_tag('I', '28')
    batch.play_tag('P', '28')

    # I, version, P
    batch.play_tag('I', '31')
    batch.play_tag('P', '33')

    # I, version, version, P
    batch.play_tag('I', '34')
    batch.play_tag('P', '35')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
