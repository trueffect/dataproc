# ------------------------------------------------------------------------------
#
# Verify DAILY end-to-end data for creative versions
#
# NOTE: For daily verification, it needs to be run 2 hrs and 5 min after the utc day.
#
# ------------------------------------------------------------------------------


import sys
import datetime
import verify

# creative id to verify
creative_id = '5982897'
hours_ago_to_verify = 24

# ------------------------------------------------------------------------------

# get the utc timestamp for 2 hours ago (the time it takes for transactions to show up in edw)
hour_id = datetime.datetime.strftime(datetime.datetime.utcnow() - datetime.timedelta(hours=hours_ago_to_verify), '%Y%m%d%H')
day_id = hour_id[:8]    # first 8 chars of hour_id
hour = hour_id[8:10]    # last 2 chars of hour_id
print('########################################')
print('=== Running DAILY verification ===')
print('now:     ' + str(datetime.datetime.now()))
print('hour_id: ' + hour_id)
print('day_id:  ' + day_id)
print('hour:    ' + hour)

# run DAILY verification
result = verify.run_daily_verification(creative_id, day_id)
print('result: ' + str(result))

# return the test result
sys.exit(int(result))

# ------------------------------------------------------------------------------
