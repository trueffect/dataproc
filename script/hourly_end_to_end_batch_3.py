# ------------------------------------------------------------------------------
#
# Batch 2 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import lib
import batch

# ------------------------------------------------------------------------------
def run_batch():
    # increment the creative version
    lib.inc_creative_version('5982897', '5982878', 'rat-beetle_259x195.jpg', '5982894')
    batch.inc_version()

    # ----------------------------------------------------------
    # unassociated I,C,P
    # ----------------------------------------------------------

    batch.play_tag('I', '04')
    batch.play_tag('C', '05')
    batch.play_tag('P', '06')

    # ----------------------------------------------------------
    # associated I,C,P transactions (CT)
    # ----------------------------------------------------------

    # ICP same version
    batch.play_tag('I', '11')
    batch.play_tag('C', '11')
    batch.play_tag('P', '11')

    # I, version, C, version, P
    batch.play_tag('C', '13')
    batch.play_tag('I', '14')
    batch.play_tag('P', '15')

    # I, version, CP
    batch.play_tag('C', '16')
    batch.play_tag('P', '16')
    batch.play_tag('I', '17')

    # IC, version, P
    batch.play_tag('P', '19')
    batch.play_tag('I', '20')
    batch.play_tag('C', '20')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
