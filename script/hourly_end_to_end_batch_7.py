# ------------------------------------------------------------------------------
#
# Batch 4 for Associated and Unassociated Data, including CT and VT
# NOTE: this is intended to be run at hour 23 of the day, after batch 3 runs in the crontab.
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import lib
import batch

# ------------------------------------------------------------------------------
def run_batch():
    # increment the creative version
    lib.inc_creative_version('5982897', '5982878', 'rat-beetle_259x195.jpg', '5982894')
    batch.inc_version()

    # ----------------------------------------------------------
    # unassociated I,C,P
    # ----------------------------------------------------------

    batch.play_tag('I', '01')
    batch.play_tag('C', '02')
    batch.play_tag('P', '03')

    # ----------------------------------------------------------
    # associated I,C,P transactions (CT)
    # ----------------------------------------------------------

    # ICP same version
    batch.play_tag('I', '04')
    batch.play_tag('C', '04')
    batch.play_tag('P', '04')

    # ----------------------------------------------------------
    # associated I,P transactions (VT)
    # ----------------------------------------------------------

    # IP same version
    batch.play_tag('I', '05')
    batch.play_tag('P', '05')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
