# ----------------------------------------------------------
import os
import socket
# set oracle env vars if the host is the qa server
if socket.gethostname() == 'mercury.trueffect.com':
    #print('Setting ORACLE_HOME and LD_LIBRARY_PATH env vars..')
    os.environ['ORACLE_HOME'] = "/app/oracle/11g"
    os.environ['LD_LIBRARY_PATH'] = "/app/oracle/11g/lib"
import cx_Oracle
import datetime

import json

import sys
# path to my project
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# add 'ad-server/script/lib' to system path
#sys.path.append(MY_REPO_DIR + os.sep + 'script' + os.sep + 'lib')

# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
MY_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

# add to 'automated-tests/common/lib' to system path
sys.path.append(MY_PROJECT_DIR + os.sep + 'common' + os.sep + 'lib')

print('--------------------------------------------------------------------------------')
print('MY_REPO_DIR: ' + MY_REPO_DIR)
print('MY_PROJECT_DIR: ' + MY_PROJECT_DIR)

# import libs from the common repo
import cms_api
import cassandra_api as cas

# oracle data source names
dsnTFA = "te_xls/3t3xl$" + "@runamuck.trueffect.com:1521" + "/tfaqa.trueffect.com"
dsnEDW = "dev_user/d3vu53r" + "@runamuck.trueffect.com:1521" + "/edwqa.trueffect.com"
dsnTDM = "dv_owner/dvm$0wn3r" + "@runamuck.trueffect.com:1521" + "/tdmqa.trueffect.com"


# ----------------------------------------------------------
#   Creative Versioning Helpers
# ----------------------------------------------------------


# ----------------------------------------------------------
def inc_creative_version(creative_id, campaign_id, filename, creative_group_id):
    print('==>  in lib.inc_creative_version(creative_id, campaign_id, filename, creative_group_id)')
    cms_api.increment_creative_version(creative_id, campaign_id, filename, creative_group_id)
    set_creative_version_active(creative_id)
    print('<== out lib.inc_creative_version(creative_id, campaign_id, filename, creative_group_id)')
    return cms_api.get_creative_version(creative_id)

# ----------------------------------------------------------
def set_creative_version_active(creative_id, version_number=0):
    print('--------------------')
    print('==>  in lib.set_creative_version_active(creative_id, version_number=0)')
    print('Setting creative version active: ' + str(creative_id) + ' ' + str(version_number))
    # if version_number is not specified, use the latest version
    if version_number == 0:
        version_number = get_creative_version(creative_id)+1

    # set the start_date and is_date_set columns
    sql = "update te_xls.CREATIVE_VERSION " \
          "set START_DATE = sysdate, IS_DATE_SET = 1 " \
          "where creative_id = " + creative_id + " and VERSION_NUMBER = " + str(version_number)

    # execute the update on the db
    conn = None
    curs = None

    # connect and run sql
    try:
        print('Connecting to Oracle: ' + dsnTFA)
        conn = cx_Oracle.Connection(dsnTFA)     # connect to db
        try:
            print('Getting cursor')
            curs = conn.cursor()                # get cursor object
            print('Executing SQL: ' + sql)
            curs.execute(sql)                   # execute sql
            print('Committing')
            conn.commit()
        finally:
            if curs is not None:
                print('Closing cursor')
                curs.close()                    # close cursor
    finally:
        print('Closing connection')
        if conn is not None:
            conn.close()                        # close connection

    # all done
    print('<== out lib.set_creative_version_active(creative_id, version_number=0)')
    return

# ------------------------------------------------------------------------------
def get_utc_hour_id():
    return str(datetime.datetime.utcnow().strftime('%Y%m%d%H'))

# ----------------------------------------------------------
def db_fetchone(sql, dsn, int_zero_based_index):
    conn = None
    curs = None
    #print('sql: ' + sql)
    # connect and run sql
    try:
        print('--------------------')
        print('==>  in lib.db_fetchone(sql, dsn, int_zero_based_index)')
        print('Connecting to oracle: ' + dsn)
        conn = cx_Oracle.Connection(dsn)        # connect to db
        try:
            print('Getting cursor')
            curs = conn.cursor()                # get cursor object
            curs.execute(sql)                   # execute sql
            print('Executing sql: ' + sql)
            query_result = curs.fetchone()[int_zero_based_index]   # fetchone() returns a single tuple, [0] is the value
            print('SQL Result: ' + str(query_result))
        except Exception as e:
            print('Unexpected Error running Query!')
            print(str(e))
        finally:
            if curs is not None:
                print('Closing cursor')
                curs.close()                    # close cursor
    finally:
        if conn is not None:
            print('Closing connection')
            conn.close()                        # close connection

    # all done
    print('<== out lib.db_fetchone(sql, dsn, int_zero_based_index)')
    return query_result

# ----------------------------------------------------------
def db_fetchall(sql, dsn):
    conn = None
    curs = None

    # connect and run sql
    try:
        print('==>  in lib.db_fetchall(sql, dsn)')
        print('Connecting to oracle: ' + dsn)
        conn = cx_Oracle.Connection(dsn)     # connect to db
        try:
            print('Getting cursor')
            curs = conn.cursor()                # get cursor object
            print('Executing sql: ' + sql)
            curs.execute(sql)                   # execute sql
            query_result = curs.fetchall()
            print('SQL Result: ' + str(query_result))
        except Exception as e:
            print('Unexpected Error running Query!')
            print(str(e))
        finally:
            if curs is not None:
                print('Closing cursor')
                curs.close()                    # close cursor
    finally:
        if conn is not None:
            print('Closing connection')
            conn.close()                        # close connection

    # all done
    print('<== out lib.db_fetchall(sql, dsn)')
    return query_result

# ----------------------------------------------------------
def get_creative_version(creative_id):
    return cms_api.get_creative_version(creative_id)

# ----------------------------------------------------------
def get_creative_version_latest_by_hour_id(creative_id, hour_id):
    print('==>  in lib.creative_version_latest_by_hour_id(creative_id, hour_id)')
    sql = "select VERSION_NUMBER " \
          "from te_xls.CREATIVE_VERSION " \
          "where CREATIVE_ID = '" + creative_id + "' " \
          "and ALIAS like '%_" + hour_id + "' " \
          "and ROWNUM = 1 " \
          "order by version_number desc"
    print('<== out lib.creative_version_latest_by_hour_id(creative_id, hour_id)')
    return int(db_fetchone(sql, dsnTFA, 0))     # 0 = get first row

# ----------------------------------------------------------
def get_creative_version_latest_by_day_id(creative_id, day_id):
    print('==>  in lib.get_creative_version_latest_by_day_id(creative_id, day_id)')
    sql = "select VERSION_NUMBER " \
          "from te_xls.CREATIVE_VERSION " \
          "where CREATIVE_ID = '" + creative_id + "' " \
          "and ALIAS like '%_" + day_id + "%' " \
          "and ROWNUM = 1 " \
          "order by version_number desc"
    print('<== out lib.get_creative_version_latest_by_day_id(creative_id, day_id)')
    return int(db_fetchone(sql, dsnTFA, 0))     # 0 = get first row


# ----------------------------------------------------------
#   Query STAGE Tables
# ----------------------------------------------------------

# ----------------------------------------------------------
def get_edw_stg_ci_tran_hr(creative_id, hour_id):
    print('==>  in lib.get_edw_stg_ci_tran_hr(creative_id, hour_id)')
    sql = "select impression_qty, click_qty, creative_version " \
          "from ods_owner.stg_ci_tran_hr " \
          "where creative_id = " + creative_id + " " \
          "and hour_id = " + hour_id + " " \
          "order by creative_version desc, impression_qty"
    print('<== out lib.get_edw_stg_ci_tran_hr(creative_id, hour_id)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_stg_ping_assoc_tran_hr(creative_id, hour_id):
    print('==>  in lib.get_edw_stg_ping_assoc_tran_hr(creative_id, hour_id)')
    sql = "select vt_tran_qty, vt_revenue_amt, ct_tran_qty, ct_revenue_amt, creative_version " \
          "from ods_owner.stg_ping_assoc_tran_hr " \
          "where creative_id = " + creative_id + " " \
          "and hour_id = " + hour_id + " " \
          "order by creative_version desc"
    print('<== out lib.get_edw_stg_ping_assoc_tran_hr(creative_id, hour_id)')
    return db_fetchall(sql, dsnEDW)


# ----------------------------------------------------------
#   Query FACT Tables
# ----------------------------------------------------------

# ----------------------------------------------------------
def get_edw_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid):
    print('==>  in lib.get_edw_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)')
    #sql = "select f.day_timezone_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
    sql = "select f.day_timezone_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias " \
          "from biw_owner.f_ci_tran_dy_cid f," \
          "biw_owner.d_ci d," \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.day_timezone_id like '" + f_day_timezone_id_dayid + "%' " \
          "order by f.day_timezone_id desc, f.creative_version desc"
    print('<== out lib.get_edw_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_f_ci_tran_hr(d_creative_id, f_tran_hour_id):
    print('==>  in lib.get_edw_f_ci_tran_hr(d_creative_id, f_tran_hour_id)')
    sql = "select f.tran_hour_id, f.impression_qty, f.click_qty, f.tran_month_id, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
          "from biw_owner.f_ci_tran_hr f, " \
          "biw_owner.d_ci d, " \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.tran_hour_id = " + f_tran_hour_id + " " \
          "order by f.tran_hour_id desc, f.creative_version desc"
    print('<== out lib.get_edw_f_ci_tran_hr(d_creative_id, f_tran_hour_id)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id):
    print('==>  in lib.get_edw_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id)')
    sql = "select f.tran_hour_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
          "from biw_owner.f_ci_tran_hr_cid f, " \
          "biw_owner.d_ci d, " \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.tran_hour_id = " + f_tran_hour_id + " " \
          "order by f.tran_hour_id desc, f.creative_version desc"
    print('<== out lib.get_edw_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid):
    print('==>  in lib.get_edw_f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)')
    #sql = "select f.day_timezone_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
    sql = "select f.day_timezone_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias " \
          "from biw_owner.f_ping_assoc_tran_dy_cid f, " \
          "biw_owner.d_ci d, " \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.day_timezone_id like '" + f_day_timezone_id_dayid + "%' " \
          "order by f.day_timezone_id desc, f.creative_version desc"
    print('<== out lib.get_edw_f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id):
    print('==>  in lib.get_edw_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id)')
    sql = "select f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
          "from biw_owner.f_ping_assoc_tran_hr f, " \
          "biw_owner.d_ci d, " \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.tran_hour_id = " + f_tran_hour_id + " " \
          "order by f.tran_hour_id desc, f.creative_version desc"
    print('<== out lib.get_edw_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id)')
    return db_fetchall(sql, dsnEDW)

# ----------------------------------------------------------
def get_edw_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id):
    print('==>  in lib.get_edw_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id)')
    sql = "select f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI') " \
          "from biw_owner.f_ping_assoc_tran_hr_cid f, " \
          "biw_owner.d_ci d, " \
          "biw_owner.d_creative_version dcv " \
          "where d.creative_id = " + d_creative_id + " " \
          "and d.ci_id = f.ci_id " \
          "and f.creative_version = dcv.creative_version " \
          "and f.tran_hour_id = " + f_tran_hour_id + " " \
          "order by f.creative_version desc"
    print('<== out lib.get_edw_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id)')
    return db_fetchall(sql, dsnEDW)


# ----------------------------------------------------------
#   Realtime Streaming (TDM, Cassandra)
# ----------------------------------------------------------

# ----------------------------------------------------------
# get the high water mark for tdm
def tdm_gbl_control_refresh_agg_campaign():
    print('==>  in lib.tdm_gbl_control_refresh_agg_campaign()')
    sql = "select MIN_TIME from GBL_CONTROL where CONTROL_NAME = 'REFRESH_AGG_CAMPAIGN'"
    print('<== out lib.tdm_gbl_control_refresh_agg_campaign()')
    return str(db_fetchone(sql, dsnTDM, 0))

# ----------------------------------------------------------
# get I,C counts for the campaign for the given day
def tdm_agg_campaign(campaign_id, day_id):
    print('==>  in lib.tdm_agg_campaign(campaign_id, day_id)')
    sql = "select IMPRESSIONS, CLICKS from AGG_CAMPAIGN where CAMPAIGN_ID = '" + campaign_id + "' and day_id = '" + day_id + "'"
    print('<== out lib.tdm_agg_campaign(campaign_id, day_id)')
    return db_fetchall(sql, dsnTDM)

# ----------------------------------------------------------
# verifies the current I,C counts for a given campaign
def verify_tdm_hourly(campaign_id):
    print('==>  in lib.verify_tdm_hourly(campaign_id)')
    # get the current day_id and hour from the high water mark
    min_time = tdm_gbl_control_refresh_agg_campaign()
    day_id = min_time[:8]
    hr = min_time[8:]
    print('min_time: ' + min_time)
    print('day_id: ' + day_id)
    print('hr: ' + hr)

    # calc the expected counts
    i_expected = int(hr) * 30 + 30
    c_expected = int(hr) * 21 + 21
    print('i_expected: ' + str(i_expected))
    print('c_expected: ' + str(c_expected))

    # get the actual counts from TDM AGG_CAMPAIGN table
    counts = tdm_agg_campaign(campaign_id, day_id)
    print(counts)
    i_actual = counts[0][0]
    c_actual = counts[0][1]
    print('i_actual: ' + str(i_actual))
    print('c_actual: ' + str(c_actual))

    # verify counts
    i_verify = True
    c_verify = True
    if i_expected != i_actual:
        i_verify = False
        print('IMPRESSION count mismatch!  Actual = ' + str(i_actual) + ', Expected = ' + str(i_expected))
    else:
        print('PASS - Impression count verified')
    if c_expected != c_actual:
        c_verify = False
        print('CLICKS count mismatch!  Actual = ' + str(c_actual) + ', Expected = ' + str(c_expected))
    else:
        print('PASS - Click count verified')

    print('<== out lib.verify_tdm_hourly(campaign_id)')

# ----------------------------------------------------------
def verify_realtime_counts(agency_id, campaign_id):
    print('==>  in lib.verify_realtime_counts(agency_id, campaign_id)')
    # get the current UTC time
    utc_hr_id = get_utc_hour_id()       # utc hour_id
    print('utc_hr_id: ' + utc_hr_id)
    utc_dy = utc_hr_id[:8]              # utc day
    print('utc_dy: ' + utc_dy)
    utc_hr = utc_hr_id[8:]              # utc hour
    print('utc_hr: ' + utc_hr)

    # get high water mark for refresh_agg_campaign
    hw_tdm = tdm_gbl_control_refresh_agg_campaign()
    print('hw_tdm: ' + str(hw_tdm))
    hw_dy = hw_tdm[:8]
    print('hw_dy: ' + hw_dy)
    hw_hr = hw_tdm[8:]
    print('hw_hr: ' + hw_hr)

    # determine the day and hour to verify
    dy_to_verify = hw_dy
    hr_to_verify = hw_hr
    hr_id_to_verify = hw_tdm
    # if the highwater mark and the utc time are on different days, use the utc date/time
    if str(hw_dy) != str(utc_dy):
        print('High water mark is NOT in the current UTC day!')
        dy_to_verify = utc_dy
        hr_to_verify = utc_hr
        hr_id_to_verify = utc_hr_id
    else:
        print('High water mark IS in the current UTC day!')
    print('dy_to_verify: ' + str(dy_to_verify))
    print('hr_to_verify: ' + str(hr_to_verify))
    print('hr_id_to_verify: ' + str(hr_id_to_verify))

    # expected tdm counts
    tdm_i_count_expected = int(hr_to_verify) * 30 + 30
    tdm_c_count_expected = int(hr_to_verify) * 21 + 21

    # get actual tdm counts
    print('Getting counts from TDM..')
    tdm_counts = tdm_agg_campaign(campaign_id, dy_to_verify)
    print('tdm_counts: ' + str(tdm_counts))
    if tdm_counts:
        print(' - got counts')
        tdm_i_count = tdm_counts[0][0]
        tdm_c_count = tdm_counts[0][1]
    else:
        print(' - no counts')
        tdm_i_count = 0
        tdm_c_count = 0

    # get actual cassandra counts
    print('Getting counts from cassandra..')
    cas_counts = cas.get_cassandra_counts(campaign_id, hr_id_to_verify)
    cas_i_count = cas_counts[0]
    cas_c_count = cas_counts[1]

    # get the expected cassandra counts
    cas_i_count_expected = cas_i_count
    cas_c_count_expected = cas_c_count

    # expected api counts
    api_i_count_expected = tdm_i_count + cas_i_count
    api_c_count_expected = tdm_c_count + cas_c_count

    # get actual api counts
    print('Getting counts from API..')
    metric = cms_api.get_agency_metrics_by_campaigns_current_day(agency_id, campaign_id)
    api_i_count = metric[0]
    api_c_count = metric[1]

    # print counts
    print('----------------------------------------')
    print('highwater_mark: ' + str(hw_tdm))
    print('current utc hour_id: ' + str(utc_hr_id))
    print('----------------------------------------')
    print('tdm_i_count: ' + str(tdm_i_count))
    print('tdm_c_count: ' + str(tdm_c_count))
    print('tdm_i_count_expected: ' + str(tdm_i_count_expected))
    print('tdm_c_count_expected: ' + str(tdm_c_count_expected))
    print('----------------------------------------')
    print('cas_i_count: ' + str(cas_i_count))
    print('cas_c_count: ' + str(cas_c_count))
    print('cas_i_count_expected: ' + str(cas_i_count_expected))
    print('cas_c_count_expected: ' + str(cas_c_count_expected))
    print('----------------------------------------')
    print('api_i_count: ' + str(api_i_count))
    print('api_c_count: ' + str(api_c_count))
    print('api_i_count_expected: ' + str(api_i_count_expected))
    print('api_c_count_expected: ' + str(api_c_count_expected))

    # verify results
    print('----------------------------------------')
    result_bit = 0
    if api_i_count_expected != api_i_count:
        result_bit += 1
        print('FAIL: Impression Count Mismatch!')
        print(' - actual:   ' + str(api_i_count))
        print(' - expected: ' + str(api_i_count_expected))
        print(' - delta:    ' + str(api_i_count_expected - api_i_count))
    else:
        print('Pass: Impression Count Matches!')
    if api_c_count_expected != api_c_count:
        result_bit += 2
        print('FAIL: Click Count Mismatch!')
        print(' - actual:   ' + str(api_c_count))
        print(' - expected: ' + str(api_c_count_expected))
        print(' - delta:    ' + str(api_c_count_expected - api_c_count))
    else:
        print('Pass: Click Count Matches!')

    # verify timestamps
    print('----------------------------------------')
    result_bit_ts = verify_realtime_timestamps()

    # return results
    print('<== out lib.verify_realtime_counts(agency_id, campaign_id)')
    return result_bit + result_bit_ts

# ----------------------------------------------------------
def verify_realtime_timestamps():
    print('==>  in lib.verify_realtime_timestamps()')
    # get the actual api timestamps
    actual_tdm = cms_api.get_agencies_metric_timestamp_tdm()
    print('actual_tdm: ' + str(actual_tdm))
    actual_cass = cms_api.get_agencies_metric_timestamp_cass()
    print('actual_cass: ' + str(actual_cass))

    # get the expected tdm and cassandra timestamps
    tdm_ts_tdm = tdm_gbl_control_refresh_agg_campaign()
    print('tdm_ts_tdm: ' + str(tdm_ts_tdm))
    cass_ts_cass = cas.get_cassandra_timestamp()
    print('cass_ts_cass: ' + str(cass_ts_cass))

    # convert timestamps to same format
    # tdm
    tdm_year = tdm_ts_tdm[:4]
    tdm_mon = tdm_ts_tdm[4:6]
    tdm_day = tdm_ts_tdm[6:8]
    tdm_hr = tdm_ts_tdm[8:10]
    expected_tdm = tdm_year + '-' + tdm_mon + '-' + tdm_day + 'T' + tdm_hr + ':00:00.000'
    print('expected_tdm: ' + expected_tdm)
    # cassandra
    cass_ymd = cass_ts_cass[:10]
    cass_hr = cass_ts_cass[11:13]
    cass_min = cass_ts_cass[14:16]
    cass_sec = cass_ts_cass[17:19]
    expected_cass = cass_ymd + 'T' + cass_hr + ':' + cass_min + ':' + cass_sec + '.000'
    print('expected_cass: ' + expected_cass)

    # verify actual and expected values match
    return_bit = 0;
    print('----------------------------------------')
    if str(expected_tdm) != str(expected_tdm):
        print('FAIL: tdm timestamp mismatch!')
        return_bit += 1
    else:
        print('Pass: tdm timestamp matches')
    if str(expected_cass) != str(expected_cass):
        print('FAIL: cassandra timestamp mismatch!')
        return_bit += 2
    else:
        print('Pass: cassandra timestamp matches')

    # returns zero if no validation errors
    print('<== out lib.verify_realtime_timestamps()')
    return return_bit

# ----------------------------------------------------------------------------------------------------------------------
#   Intraday realtime metrics
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------
def verify_realtime_counts_intraday_campaign(campaign_id):
    print('==>  in lib.verify_realtime_counts_intraday_campaign(campaign_id)')
    print('---------- Verifying intraday realtime data for Campaign ----------')
    print(' - campaign_id: ' + str(campaign_id))

    # get the current UTC time
    utc_hr = get_utc_hour_id()      # utc hour
    utc_dy = utc_hr[:8]             # utc day
    print(' - utc_dy: ' + utc_dy)

    # get list of lists from API for the current UTC day [hour_id, impressions, clicks]
    api_lol = cms_api.get_campaign_metric_intraday(campaign_id, utc_dy)
    # print(api_lol)

    # get list of lists from Cassandra for the current UTC day [hour_id, impressions, clicks]
    cass_lol = cas.get_cassandra_counts_intraday(campaign_id, utc_dy)
    # print(cass_lol)

    # verify that the lists match
    print('----------------------------------------')
    if api_lol == cass_lol:
        test_passes = 0
        print('Pass!  API and Cassandra data match.')
    else:
        print('FAIL!  API and Cassandra data do NOT match!')
        test_passes = 1

    # returns True if test passes, False if fails
    print('<== out lib.verify_realtime_counts_intraday_campaign(campaign_id)')
    return test_passes

# ----------------------------------------------------------
def verify_realtime_counts_intraday_placements(campaign_id, placement_list):
    print('==>  in lib.verify_realtime_counts_intraday_placements(campaign_id)')
    print('---------- Verifying intraday realtime data for Placements ----------')
    print(' - campaign_id:    ' + str(campaign_id))
    print(' - placement_list: ' + str(placement_list))

    # get the current UTC time
    utc_hr = get_utc_hour_id()      # utc hour
    utc_dy = utc_hr[:8]             # utc day
    print(' - utc_dy: ' + utc_dy)

    # get list of lists from API for the current UTC day [hour_id, impressions, clicks]
    api_lol = cms_api.get_campaign_metric_intraday_placements(campaign_id, placement_list)
    print(api_lol)

    # get list of lists from Cassandra for the current UTC day [hour_id, impressions, clicks]
    cass_lol = cas.get_cassandra_counts_intraday_placement(placement_list, utc_dy, utc_hr)
    print(cass_lol)

    # verify that the lists match
    print('----------------------------------------')
    if sorted(api_lol) == sorted(cass_lol):
        test_passes = 0
        print('Pass!  API and Cassandra data match.')
    else:
        print('FAIL!  API and Cassandra data do NOT match!')
        test_passes = 1

    # returns True if test passes, False if fails
    print('<== out lib.verify_realtime_counts_intraday_placements(campaign_id)')
    return test_passes

# ----------------------------------------------------------
def verify_realtime_counts_intraday_agency(agency_id, campaign_list):
    print('==>  in lib.verify_realtime_counts_intraday_agency(agency_id)')
    print('---------- Verifying intraday realtime data for Agency ----------')
    print(' - agency_id: ' + str(agency_id))
    print(' - campaign_list: ' + str(campaign_list))

    # get the current UTC time
    utc_hr = get_utc_hour_id()      # utc hour
    utc_dy = utc_hr[:8]             # utc day
    print(' - utc_dy: ' + utc_dy)

    # get list of lists from API for the current UTC day [hour_id, impressions, clicks]
    api_lol = cms_api.get_agency_metric_intraday(agency_id, utc_dy, campaign_list)
    # print(api_lol)

    # get list of lists from Cassandra for the current UTC day [hour_id, impressions, clicks]
    cass_lol = cas.get_cassandra_counts_intraday_agency(campaign_list, utc_dy)
    # print(cass_lol)

    # verify that the lists match
    print('----------------------------------------')
    if sorted(api_lol) == sorted(cass_lol):
        test_passes = 0
        print('Pass!  API and Cassandra data match.')
    else:
        print('FAIL!  API and Cassandra data do NOT match!')
        print(str(len(api_lol)))
        print(str(len(cass_lol)))
        test_passes = 1

    # returns True if test passes, False if fails
    print('<== out lib.verify_realtime_counts_intraday_agency(agency_id)')
    return test_passes


# ----------------------------------------------------------

# campaign_id = '5982878'
# placement_list = [5982883]
# verify_realtime_counts_intraday_placements(campaign_id, placement_list)

#inc_creative_version('5982897', '5982878', 'rat-beetle_259x195.jpg', '5982894')

#verify_realtime_timestamps()


# intraday campaign
#campaign_id = '5982878'
# verify_realtime_counts(agency_id, campaign_id)
# verify_realtime_counts_intraday_campaign(campaign_id)

# # intraday placements
# campaign_id = '6566237'
# placement_list = 6566248, 6566251
# verify_realtime_counts_intraday_placements(campaign_id, placement_list)

# # intraday agency
# agency_id = '5975435'
# campaign_list = 5982878, 6566237, 6566238, 6566239, 6566240
# verify_realtime_counts_intraday_agency(agency_id, campaign_list)


#verify_tdm_hourly(campaign_id)


# # end to end creative
# campaign_id = '5982878'
# alias = 'testing'
# filename = 'rat-beetle_259x195.jpg'
# creative_id = '5982897'
# creative_group_id = '5982894'

# # test creative
# campaign_id = '5993656'
# alias = 'v1'
# filename = 'rat-bus_259x194.jpg'
# creative_id = '5993692'
# creative_group_id = '5993689'
#
# inc_creative_version(creative_id, campaign_id, filename, creative_group_id)

# get_creative_version('5982897')     # end-to-end automation
# get_creative_version('5993692')     # test creative
# get_creative_version('6452662')     # random test creative, not versioned

#inc_creative_version('5993692', campaign_id, filename, creative_group_id)

# ######################################################################################################################
