import requests
import uuid
from datetime import datetime
import time

# build request headers and cookies
headers = {
    "X-Forwarded-For": "208.186.79.94",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36",
}
cookies = {
    "PrefID": "789-346"
}

# timestamp
now = datetime.now()
automation_id = now.strftime('%Y%m%d%H%M%S')
print('----------------------------------------')
print('automation_id: ' + automation_id)

# uuid
ptxnid = str(uuid.uuid4())
print('ptxnid: ' + ptxnid)
print('----------------------------------------')

# list of urls to request
#   creative_id = 6395024
#   campaign_id = 6394755
urls = [
    'https://ext.adlegend.net/template?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id),
    'https://ext.adlegend.net/image?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id),
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=start&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=firstQuartile&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=midpoint&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=thirdQuartile&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=mute&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=unmute&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=pause&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=resume&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=complete&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=rewind&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=fullscreen&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=expand&rdm=78980',
    'https://ext.adlegend.net/image_htmlping?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&af=6394800&txntime=txntime&ml_usec=78980&ml_pkgkw=-%253A%2522%2522&ml_pbi=-6394797&ml_camp=6394755&ml_crid=6395024&ptxnid=' + ptxnid + '&group=inBanner&event=collapse&rdm=78980',
    'https://ext.adlegend.net/click?spacedesc=6394797_5624134_2x2_5624136_6394797&automation_id=' + str(automation_id) + '&random=' + ptxnid
]

# iterate each url in list
for url in urls:
    # get the current time, set the txntime query string param
    now = datetime.now()
    txntime = now.strftime('%Y.%m.%d.%H.%M.%S')
    url = url.replace('&txntime=txntime&', '&txntime=' + txntime + '&')
    print('----------------------------------------')
    print('txntime: ' + txntime)
    print(url)

    # fire the request
    response = requests.get(url, headers=headers, cookies=cookies)

    # get response status code (ie. 200, 302, etc.)
    response_status_code = response.status_code
    print('response_status_code: ' + str(response_status_code))

    # sleep a second...
    print(' - sleeping a second...')
    time.sleep(1)

print('All done!')
