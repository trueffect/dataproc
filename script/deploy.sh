#!/usr/bin/env bash

# remove repos
rm -rf dataproc
rm -rf common

# create repo dirs
mkdir dataproc
mkdir common

# clone repos
git clone "https://bitbucket.org/trueffect/dataproc.git" /home/prodman/simulateTraffic/end_to_end/dataproc
git clone "https://bitbucket.org/trueffect/common.git" /home/prodman/simulateTraffic/end_to_end/common
