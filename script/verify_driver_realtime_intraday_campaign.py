# ------------------------------------------------------------------------------
#
# Verify realtime intraday metrics for a Campaign
#
# ------------------------------------------------------------------------------

import sys
import lib

# campaign to get counts for
campaign_id = '5982878'

print('########################################')
print('=== Running realtime INTRADAY verification for Campaign ===')
print('campaign_id: ' + campaign_id)

result = lib.verify_realtime_counts_intraday_campaign(campaign_id)
print('result: ' + str(result))

# return the test result
sys.exit(int(result))

# ------------------------------------------------------------------------------
