# ------------------------------------------------------------------------------
#
# Verify realtime intraday metrics for an Agency and list of campaigns
#
# ------------------------------------------------------------------------------

import sys
import lib

# agency and list of campaigns to get counts for
agency_id = '5975435'
campaign_list = 5982878, 6566237, 6566238, 6566239, 6566240

print('########################################')
print('=== Running realtime INTRADAY verification for Agency ===')
print(' - agency_id: ' + agency_id)
print(' - campaign_list: ' + str(campaign_list))

result = lib.verify_realtime_counts_intraday_agency(agency_id, campaign_list)
print('result: ' + str(result))

# return the test result
sys.exit(int(result))

# ------------------------------------------------------------------------------
