# ------------------------------------------------------------------------------
#
# Batch 3 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import lib
import batch

# ------------------------------------------------------------------------------
def run_batch():
    # increment the creative version
    lib.inc_creative_version('5982897', '5982878', 'rat-beetle_259x195.jpg', '5982894')
    batch.inc_version()

    # ----------------------------------------------------------
    # unassociated I,C,P
    # ----------------------------------------------------------

    batch.play_tag('I', '07')
    batch.play_tag('C', '08')
    batch.play_tag('P', '09')

    # ----------------------------------------------------------
    # associated I,C,P transactions (CT)
    # ----------------------------------------------------------

    # ICP same version
    batch.play_tag('I', '12')
    batch.play_tag('C', '12')
    batch.play_tag('P', '12')

    # I, version, C, version, P
    batch.play_tag('P', '13')
    batch.play_tag('C', '14')
    batch.play_tag('I', '15')

    # I, version, CP
    batch.play_tag('C', '17')
    batch.play_tag('P', '17')
    batch.play_tag('I', '18')

    # IC, version, P
    batch.play_tag('P', '20')
    batch.play_tag('I', '21')
    batch.play_tag('C', '21')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
