# ------------------------------------------------------------------------------
#
# Verify hourly and daily end-to-end data for creative versions
#
# ------------------------------------------------------------------------------

import os
import sys
import subprocess
from colorama import Fore, Style
import datetime
import time

# import /lib/lib.py
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(MY_REPO_DIR + os.sep + 'lib')
import lib

# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
AT_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

# add 'automated-tests/common/lib' to system path
sys.path.append(AT_PROJECT_DIR + os.sep + 'common' + os.sep + 'lib')

# print('--------------------------------------------------------------------------------')
# print('MY_REPO_DIR: ' + MY_REPO_DIR)
# print('AT_PROJECT_DIR: ' + AT_PROJECT_DIR)

# ------------------------------------------------------------------------------
def run_hourly_verification(creative_id, day_id, hour):
    result_bits = 0                     # to keep track of which tests fail, if any
    hour_id = day_id + hour             # YYYYMMMDD in string format
    d_creative_id = creative_id         # dim creative id
    f_tran_hour_id = hour_id            # fact table hour id

    # get the latest version of the creative for the hour to verify
    #creative_version_hr = lib.get_creative_version_latest_by_hour_id(creative_id, hour_id)
    creative_version_hr = 0

    # print out some relevant info
    print('--------------------------------------------------------------------------------')
    print(Fore.MAGENTA + '=== run_hourly_verification() ===' + Style.RESET_ALL)
    print('--------------------------------------------------------------------------------')
    print('day_id:  ' + Fore.YELLOW + day_id + Style.RESET_ALL)
    print('hour_id: ' + Fore.YELLOW + hour_id + Style.RESET_ALL)
    print('creative_id: ' + Fore.YELLOW + creative_id + Style.RESET_ALL)
    print('creative_version_hr: ' + Fore.YELLOW + str(creative_version_hr) + Style.RESET_ALL)

    # ========================================
    # Verify STG Tables
    # ========================================

    # ods_owner.stg_ci_tran_hr
    if not verify_stg_ci_tran_hr(creative_id, hour_id, creative_version_hr):
        result_bits += 1

    # ods_owner.stg_ping_assoc_tran_hr
    if not verify_stg_ping_assoc_tran_hr(creative_id, hour_id, creative_version_hr):
        result_bits += 2

    # ========================================
    # Verify FACT Tables
    # ========================================

    # biw_owner.f_ci_tran_hr
    if not verify_f_ci_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 4

    # biw_owner.f_ci_tran_hr_cid
    if not verify_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 8

    # biw_owner.f_ping_assoc_tran_hr
    if not verify_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 16

    # biw_owner.f_ping_assoc_tran_hr_cid
    if not verify_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 32

    # overall results of all tests above
    print('----------------------------------------')
    print(Fore.CYAN + 'Results for run_hourly_verification(): ' + str(result_bits) + Style.RESET_ALL)
    return result_bits

# ------------------------------------------------------------------------------
def verify_stg_ci_tran_hr(creative_id, hour_id, creative_version_hr):
    # impression_qty, click_qty, creative_version
    if hour_id[8:] != '23':
        expected = [
            (0, 1, 0),
            (1, 0, 0)
        ]
    else:
        expected = [
            (0, 2, 0),
            (2, 0, 0)
        ]
    actual = lib.get_edw_stg_ci_tran_hr(creative_id, hour_id)
    return verify_result('ods_owner.stg_ci_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_stg_ping_assoc_tran_hr(creative_id, hour_id, creative_version_hr):
    # vt_tran_qty, vt_revenue_amt, ct_tran_qty, ct_revenue_amt, creative_version
    hr_id = hour_id[8:]
    if hr_id == '23':
        expected = [
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0),
            (2, 0, 0, 0, 0)
        ]
    elif hr_id == '00':
        expected = [
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0)
        ]
    else:
        expected = [
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0),
            (1, 0, 0, 0, 0)
        ]
    actual = lib.get_edw_stg_ping_assoc_tran_hr(creative_id, hour_id)
    return verify_result('ods_owner.stg_ping_assoc_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ci_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
    year_mo = int(f_tran_hour_id[:6])
    offset = get_tz_delta() * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    # f.tran_hour_id, f.impression_qty, f.click_qty, f.tran_month_id, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    if f_tran_hour_id[8:] != '23':
        expected = [
            ('?')
        ]
    else:
        expected = [
            ('?')
        ]
    actual = lib.get_edw_f_ci_tran_hr(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ci_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
    offset = get_tz_delta() * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    # f.tran_hour_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    if f_tran_hour_id[8:] != '23':
        expected = [
            ('?')
        ]
    else:
        expected = [
            ('?')
        ]
    actual = lib.get_edw_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ci_tran_hr_cid', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
    previous_hour = hour_id_minus_hours(f_tran_hour_id, 1)
    offset = get_tz_delta() * -1
    offset2 = (get_tz_delta()-1) * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    tran_hr2 = hour_id_minus_hours(f_tran_hour_id, offset2)
    # f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    hr_id = f_tran_hour_id[8:]
    if hr_id == '23':
        expected = [
            ('?')
        ]
    elif hr_id == '00':
        expected = [
            ('?')
        ]
    else:
        expected = [
            ('?')
        ]
    actual = lib.get_edw_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ping_assoc_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
    previous_hour = hour_id_minus_hours(f_tran_hour_id, 1)
    offset = get_tz_delta() * -1
    offset2 = (get_tz_delta()-1) * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    tran_hr2 = hour_id_minus_hours(f_tran_hour_id, offset2)
    # f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    hr_id = f_tran_hour_id[8:]
    if hr_id == '23':
        expected = [
            ('?')
        ]
    elif hr_id == '00':
        expected = [
            ('?')
        ]
    else:
        expected = [
            ('?')
        ]
    actual = lib.get_edw_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ping_assoc_tran_hr_cid', actual, expected, False)


# ------------------------------------------------------------------------------
def run_daily_verification(creative_id, day_id):
    result_bits = 0                     # to keep track of which tests fail, if any
    d_creative_id = creative_id         # dim creative id
    f_day_timezone_id_dayid = day_id    # fact table timezone day id

    # get the latest version of the creative for the day to verify
    creative_version_dy = lib.get_creative_version_latest_by_day_id(creative_id, day_id)

    # print out some relevant info
    print('--------------------------------------------------------------------------------')
    print(Fore.MAGENTA + '=== run_daily_verification() ===' + Style.RESET_ALL)
    print('--------------------------------------------------------------------------------')
    print('day_id:  ' + Fore.YELLOW + day_id + Style.RESET_ALL)
    print('creative_id: ' + Fore.YELLOW + creative_id + Style.RESET_ALL)
    print('creative_version_dy: ' + Fore.YELLOW + str(creative_version_dy) + Style.RESET_ALL)

    # ========================================
    # Verify FACT Tables
    # ========================================

    # biw_owner.f_ci_tran_dy_cid
    if not verify_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
        result_bits += 1

    # biw_owner.f_ping_assoc_tran_dy_cid
    if not f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
        result_bits += 2

    # overall results of all tests above
    print('----------------------------------------')
    print(Fore.CYAN + 'Results for run_daily_verification(): ' + str(result_bits) + Style.RESET_ALL)
    return result_bits

# ------------------------------------------------------------------------------
def verify_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
    day_id = f_day_timezone_id_dayid
    next_day_id = day_id_plus_days(day_id, 1)
    hr_12 = int(str(day_id) + str('12'))
    hr_07 = int(str(day_id) + str('07'))
    # f.day_timezone_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias
    expected = [
        ('?')
    ]
    actual = lib.get_edw_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)
    return verify_result('biw_owner.f_ci_tran_dy_cid', actual, expected, False)

# ------------------------------------------------------------------------------
def f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
    day_id = f_day_timezone_id_dayid
    previous_day_id = day_id_minus_days(day_id, 1)
    next_day_id = day_id_plus_days(day_id, 1)
    hr_12 = int(str(day_id) + str('12'))
    hr_07 = int(str(day_id) + str('07'))
    # f.day_timezone_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias
    expected = [
        ('?')
    ]
    actual = lib.get_edw_f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)
    return verify_result('biw_owner.f_ping_assoc_tran_dy_cid', actual, expected, False)


# ------------------------------------------------------------------------------
def verify_result(test_description, actual_result, expected_result, log_results_on_pass=True):
    print('----------------------------------------')
    print('Verifying ' + test_description + ' ...')
    if actual_result != expected_result:
        print(' - result: ' + Fore.RED + 'FAIL' + Style.RESET_ALL)
        result_passed = False
    else:
        print(' - result: ' + Fore.GREEN + 'Pass' + Style.RESET_ALL)
        result_passed = True
    if log_results_on_pass or not result_passed:
        bashCommand = AT_PROJECT_DIR + os.sep + 'common' + os.sep + 'util' + os.sep + 'diff_to_terminal.sh -s "' + str(actual_result) + '" "' + str(expected_result) + '"'
        output = subprocess.check_output(bashCommand, shell=True).decode('ascii').strip()
        print(output)
    return result_passed

# ------------------------------------------------------------------------------
def hour_id_minus_hours(hour_id_in, hours):
    dt_in = datetime.datetime.strptime(hour_id_in, "%Y%m%d%H")
    hour_id_out = dt_in - datetime.timedelta(hours=hours)
    return hour_id_out.strftime('%Y%m%d%H')

# ------------------------------------------------------------------------------
def hour_id_plus_hours(hour_id_in, hours):
    dt_in = datetime.datetime.strptime(hour_id_in, "%Y%m%d%H")
    hour_id_out = dt_in + datetime.timedelta(hours=hours)
    return hour_id_out.strftime('%Y%m%d%H')

# ------------------------------------------------------------------------------
def day_id_minus_days(day_id_in, days):
    dt_in = datetime.datetime.strptime(day_id_in, "%Y%m%d")
    day_id_out = dt_in - datetime.timedelta(days=days)
    return day_id_out.strftime('%Y%m%d')

# ------------------------------------------------------------------------------
def day_id_plus_days(day_id_in, days):
    dt_in = datetime.datetime.strptime(day_id_in, "%Y%m%d")
    day_id_out = dt_in + datetime.timedelta(days=days)
    return day_id_out.strftime('%Y%m%d')

# ------------------------------------------------------------------------------
def get_tz_delta():
    offset = time.timezone if (time.localtime().tm_isdst == 0) else time.altzone
    return offset / 60 / 60 * -1


# ######################################################################################################################
#   UNIT TESTS
# ######################################################################################################################

do_unit_tests = False
do_unit_tests = True

if do_unit_tests:

    # day_id and hour to verify
    day_id = '20160726'
    hour = '19'

    # creative id to verify
    creative_id = '5982897'
    creative_id = '6395024'    # video

    # verify a specific hour and day
    print('########################################')
    print(datetime.datetime.now())
    run_hourly_verification(creative_id, day_id, hour)
    # while run_hourly_verification(creative_id, day_id, hour) != 0:
    #     print('Sleeping for a minute...')
    #     time.sleep(60)
    #     print('########################################')
    #     print(datetime.datetime.now())
    # print(datetime.datetime.now())
    # #run_daily_verification(creative_id, day_id)


# ------------------------------------------------------------------------------
