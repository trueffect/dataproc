#!/bin/bash
#-------------------------------------------------------------------------------
# Script: mail_util.sh
# This script sends an automated email.
# NOTE: Only known to work as prodman on runamuck2.  Doesn't work locally on Mac OS X.
# Command Line Arguments:
#    1) email 'body'
#    2) email 'subject'
#    3) 'to' list
# Example: ./mail_util.sh "Hi There!  Hope you're having a great day!  Love, Prodman" "Test email from mail_util.sh" "wbutler@trueffect.com"
#-------------------------------------------------------------------------------

# verify correct number of command line arguments
if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    echo "USAGE: mail_util.sh \"body text\" \"subject text\" \"to list\""
fi

# call the 'mail' command
echo -e "$1" | mail -s "$2" "$3"