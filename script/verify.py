# ------------------------------------------------------------------------------
#
# Verify hourly and daily end-to-end data for creative versions
#
# ------------------------------------------------------------------------------

import os
import sys
import subprocess
import datetime
import time

# import /lib/lib.py
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(MY_REPO_DIR + os.sep + 'lib')
import lib

# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
AT_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

# add 'automated-tests/common/lib' to system path
sys.path.append(AT_PROJECT_DIR + os.sep + 'common' + os.sep + 'lib')

# print('--------------------------------------------------------------------------------')
# print('MY_REPO_DIR: ' + MY_REPO_DIR)
# print('AT_PROJECT_DIR: ' + AT_PROJECT_DIR)

# ------------------------------------------------------------------------------
def run_hourly_verification(creative_id, day_id, hour):
    result_bits = 0                     # to keep track of which tests fail, if any
    hour_id = day_id + hour             # YYYYMMMDD in string format
    d_creative_id = creative_id         # dim creative id
    f_tran_hour_id = hour_id            # fact table hour id

    # get the latest version of the creative for the hour to verify
    creative_version_hr = lib.get_creative_version_latest_by_hour_id(creative_id, hour_id)

    # print out some relevant info
    print('--------------------------------------------------------------------------------')
    print('=== run_hourly_verification() ===')
    print('--------------------------------------------------------------------------------')
    print('day_id:  ' + day_id)
    print('hour_id: ' + hour_id)
    print('creative_id: ' + creative_id)
    print('creative_version_hr: ' + str(creative_version_hr))

    # ========================================
    # Verify STG Tables
    # ========================================

    # ods_owner.stg_ci_tran_hr
    print('Verifying ods_owner.stg_ci_tran_hr')
    if not verify_stg_ci_tran_hr(creative_id, hour_id, creative_version_hr):
        result_bits += 1
    print(' - done, result_bits: ' + str(result_bits))

    # ods_owner.stg_ping_assoc_tran_hr
    print('Verifying ods_owner.stg_ping_assoc_tran_hr')
    if not verify_stg_ping_assoc_tran_hr(creative_id, hour_id, creative_version_hr):
        result_bits += 2
    print(' - done, result_bits: ' + str(result_bits))

    # ========================================
    # Verify FACT Tables
    # ========================================

    # biw_owner.f_ci_tran_hr
    print('Verifying biw_owner.f_ci_tran_hr')
    if not verify_f_ci_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 4
    print(' - done, result_bits: ' + str(result_bits))

    # biw_owner.f_ci_tran_hr_cid
    print('Verifying biw_owner.f_ci_tran_hr_cid')
    if not verify_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 8
    print(' - done, result_bits: ' + str(result_bits))

    # biw_owner.f_ping_assoc_tran_hr
    print('Verifying biw_owner.f_ping_assoc_tran_hr')
    if not verify_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 16
    print(' - done, result_bits: ' + str(result_bits))

    # biw_owner.f_ping_assoc_tran_hr_cid
    print('Verifying biw_owner.f_ping_assoc_tran_hr_cid')
    if not verify_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
        result_bits += 32
    print(' - done, result_bits: ' + str(result_bits))

    # overall results of all tests above
    print('----------------------------------------')
    print('Results for run_hourly_verification(): ' + str(result_bits))
    return result_bits

# ------------------------------------------------------------------------------
def verify_stg_ci_tran_hr(creative_id, hour_id, creative_version_hr):
    # impression_qty, click_qty, creative_version
    if hour_id[8:] != '23':
        expected = [
            (0, 7, creative_version_hr),
            (10, 0, creative_version_hr),
            (0, 7, creative_version_hr-1),
            (10, 0, creative_version_hr-1),
            (0, 7, creative_version_hr-2),
            (10, 0, creative_version_hr-2)
        ]
    else:
        expected = [
            (0, 2, creative_version_hr),
            (3, 0, creative_version_hr),
            (0, 7, creative_version_hr-1),
            (10, 0, creative_version_hr-1),
            (0, 7, creative_version_hr-2),
            (10, 0, creative_version_hr-2),
            (0, 7, creative_version_hr-3),
            (10, 0, creative_version_hr-3)
        ]
    actual = lib.get_edw_stg_ci_tran_hr(creative_id, hour_id)
    return verify_result('ods_owner.stg_ci_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_stg_ping_assoc_tran_hr(creative_id, hour_id, creative_version_hr):
    # vt_tran_qty, vt_revenue_amt, ct_tran_qty, ct_revenue_amt, creative_version
    hr_id = hour_id[8:]
    if hr_id == '23':
        expected = [
            (1, 10, 1, 10, creative_version_hr),
            (1, 10, 3, 30, creative_version_hr-1),
            (2, 20, 5, 50, creative_version_hr-2),
            (3, 30, 6, 60, creative_version_hr-3),
            (2, 20, 3, 30, creative_version_hr-4),
            (1, 10, 1, 10, creative_version_hr-5)
        ]
    elif hr_id == '00':
        expected = [
            (1, 10, 3, 30, creative_version_hr),
            (2, 20, 5, 50, creative_version_hr-1),
            (3, 30, 6, 60, creative_version_hr-2),
            (2, 20, 3, 30, creative_version_hr-4),
            (1, 10, 1, 10, creative_version_hr-5)
        ]
    else:
        expected = [
            (1, 10, 3, 30, creative_version_hr),
            (2, 20, 5, 50, creative_version_hr-1),
            (3, 30, 6, 60, creative_version_hr-2),
            (2, 20, 3, 30, creative_version_hr-3),
            (1, 10, 1, 10, creative_version_hr-4)
        ]
    actual = lib.get_edw_stg_ping_assoc_tran_hr(creative_id, hour_id)
    return verify_result('ods_owner.stg_ping_assoc_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ci_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
    year_mo = int(f_tran_hour_id[:6])
    offset = get_tz_delta() * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    # f.tran_hour_id, f.impression_qty, f.click_qty, f.tran_month_id, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    if f_tran_hour_id[8:] != '23':
        expected = [
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04')
        ]
    else:
        expected = [
            (int(f_tran_hour_id), 3, 2, year_mo, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '56'),
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 10, 7, year_mo, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + f_tran_hour_id, str(tran_hr) + '04')
        ]
    actual = lib.get_edw_f_ci_tran_hr(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ci_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
    offset = get_tz_delta() * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    # f.tran_hour_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    if f_tran_hour_id[8:] != '23':
        expected = [
            (int(f_tran_hour_id), 10, 7, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 10, 7, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 10, 7, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04')
        ]
    else:
        expected = [
            (int(f_tran_hour_id), 3, 2, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '56'),
            (int(f_tran_hour_id), 10, 7, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 10, 7, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 10, 7, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + f_tran_hour_id, str(tran_hr) + '04')
        ]
    actual = lib.get_edw_f_ci_tran_hr_cid(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ci_tran_hr_cid', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id, creative_version_hr):
    previous_hour = hour_id_minus_hours(f_tran_hour_id, 1)
    offset = get_tz_delta() * -1
    offset2 = (get_tz_delta()-1) * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    tran_hr2 = hour_id_minus_hours(f_tran_hour_id, offset2)
    # f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    hr_id = f_tran_hour_id[8:]
    if hr_id == '23':
        expected = [
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '56'),
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-5, 'v' + str(creative_version_hr-5) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    elif hr_id == '00':
        expected = [
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-5, 'v' + str(creative_version_hr-5) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    else:
        expected = [
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    actual = lib.get_edw_f_ping_assoc_tran_hr(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ping_assoc_tran_hr', actual, expected, False)

# ------------------------------------------------------------------------------
def verify_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id, creative_version_hr):
    previous_hour = hour_id_minus_hours(f_tran_hour_id, 1)
    offset = get_tz_delta() * -1
    offset2 = (get_tz_delta()-1) * -1
    tran_hr = hour_id_minus_hours(f_tran_hour_id, offset)
    tran_hr2 = hour_id_minus_hours(f_tran_hour_id, offset2)
    # f.tran_hour_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias, to_char(dcv.start_date, 'YYYYMMDDHH24MI')
    hr_id = f_tran_hour_id[8:]
    if hr_id == '23':
        expected = [
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '56'),
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-5, 'v' + str(creative_version_hr-5) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    elif hr_id == '00':
        expected = [
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-5, 'v' + str(creative_version_hr-5) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    else:
        expected = [
            (int(f_tran_hour_id), 1, 10, 3, 30, creative_version_hr, 'v' + str(creative_version_hr) + '_' + f_tran_hour_id, str(tran_hr) + '40'),
            (int(f_tran_hour_id), 2, 20, 5, 50, creative_version_hr-1, 'v' + str(creative_version_hr-1) + '_' + f_tran_hour_id, str(tran_hr) + '22'),
            (int(f_tran_hour_id), 3, 30, 6, 60, creative_version_hr-2, 'v' + str(creative_version_hr-2) + '_' + f_tran_hour_id, str(tran_hr) + '04'),
            (int(f_tran_hour_id), 2, 20, 3, 30, creative_version_hr-3, 'v' + str(creative_version_hr-3) + '_' + previous_hour, str(tran_hr2) + '40'),
            (int(f_tran_hour_id), 1, 10, 1, 10, creative_version_hr-4, 'v' + str(creative_version_hr-4) + '_' + previous_hour, str(tran_hr2) + '22')
        ]
    actual = lib.get_edw_f_ping_assoc_tran_hr_cid(d_creative_id, f_tran_hour_id)
    return verify_result('biw_owner.f_ping_assoc_tran_hr_cid', actual, expected, False)


# ------------------------------------------------------------------------------
def run_daily_verification(creative_id, day_id):
    result_bits = 0                     # to keep track of which tests fail, if any
    d_creative_id = creative_id         # dim creative id
    f_day_timezone_id_dayid = day_id    # fact table timezone day id

    # get the latest version of the creative for the day to verify
    creative_version_dy = lib.get_creative_version_latest_by_day_id(creative_id, day_id)

    # print out some relevant info
    print('--------------------------------------------------------------------------------')
    print('=== run_daily_verification() ===')
    print('--------------------------------------------------------------------------------')
    print('day_id:  ' + day_id)
    print('creative_id: ' + creative_id)
    print('creative_version_dy: ' + str(creative_version_dy))

    # ========================================
    # Verify FACT Tables
    # ========================================

    # biw_owner.f_ci_tran_dy_cid
    if not verify_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
        result_bits += 1

    # biw_owner.f_ping_assoc_tran_dy_cid
    if not f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
        result_bits += 2

    # overall results of all tests above
    print('----------------------------------------')
    print('Results for run_daily_verification(): ' + str(result_bits))
    return result_bits

# ------------------------------------------------------------------------------
def verify_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
    day_id = f_day_timezone_id_dayid
    next_day_id = day_id_plus_days(day_id, 1)
    hr_12 = int(str(day_id) + str('12'))
    hr_07 = int(str(day_id) + str('07'))
    # f.day_timezone_id, f.impression_qty, f.click_qty, dcv.creative_version, dcv.alias
    expected = [
        # ----------------------------------------------------------------------------------------------------
        # verification for UTC microstrategy reports
        # ----------------------------------------------------------------------------------------------------
        #(hr_12, 2, 2, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),
        (hr_12, 3, 2, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),
        (hr_12, 10, 7, creative_version_dy-1, 'v' + str(creative_version_dy-1) + '_' + day_id + '23'),
        (hr_12, 10, 7, creative_version_dy-2, 'v' + str(creative_version_dy-2) + '_' + day_id + '23'),
        (hr_12, 10, 7, creative_version_dy-3, 'v' + str(creative_version_dy-3) + '_' + day_id + '23'),
        (hr_12, 10, 7, creative_version_dy-4, 'v' + str(creative_version_dy-4) + '_' + day_id + '22'),
        (hr_12, 10, 7, creative_version_dy-5, 'v' + str(creative_version_dy-5) + '_' + day_id + '22'),
        (hr_12, 10, 7, creative_version_dy-6, 'v' + str(creative_version_dy-6) + '_' + day_id + '22'),
        (hr_12, 10, 7, creative_version_dy-7, 'v' + str(creative_version_dy-7) + '_' + day_id + '21'),
        (hr_12, 10, 7, creative_version_dy-8, 'v' + str(creative_version_dy-8) + '_' + day_id + '21'),
        (hr_12, 10, 7, creative_version_dy-9, 'v' + str(creative_version_dy-9) + '_' + day_id + '21'),
        (hr_12, 10, 7, creative_version_dy-10, 'v' + str(creative_version_dy-10) + '_' + day_id + '20'),
        (hr_12, 10, 7, creative_version_dy-11, 'v' + str(creative_version_dy-11) + '_' + day_id + '20'),
        (hr_12, 10, 7, creative_version_dy-12, 'v' + str(creative_version_dy-12) + '_' + day_id + '20'),
        (hr_12, 10, 7, creative_version_dy-13, 'v' + str(creative_version_dy-13) + '_' + day_id + '19'),
        (hr_12, 10, 7, creative_version_dy-14, 'v' + str(creative_version_dy-14) + '_' + day_id + '19'),
        (hr_12, 10, 7, creative_version_dy-15, 'v' + str(creative_version_dy-15) + '_' + day_id + '19'),
        (hr_12, 10, 7, creative_version_dy-16, 'v' + str(creative_version_dy-16) + '_' + day_id + '18'),
        (hr_12, 10, 7, creative_version_dy-17, 'v' + str(creative_version_dy-17) + '_' + day_id + '18'),
        (hr_12, 10, 7, creative_version_dy-18, 'v' + str(creative_version_dy-18) + '_' + day_id + '18'),
        (hr_12, 10, 7, creative_version_dy-19, 'v' + str(creative_version_dy-19) + '_' + day_id + '17'),
        (hr_12, 10, 7, creative_version_dy-20, 'v' + str(creative_version_dy-20) + '_' + day_id + '17'),
        (hr_12, 10, 7, creative_version_dy-21, 'v' + str(creative_version_dy-21) + '_' + day_id + '17'),
        (hr_12, 10, 7, creative_version_dy-22, 'v' + str(creative_version_dy-22) + '_' + day_id + '16'),
        (hr_12, 10, 7, creative_version_dy-23, 'v' + str(creative_version_dy-23) + '_' + day_id + '16'),
        (hr_12, 10, 7, creative_version_dy-24, 'v' + str(creative_version_dy-24) + '_' + day_id + '16'),
        (hr_12, 10, 7, creative_version_dy-25, 'v' + str(creative_version_dy-25) + '_' + day_id + '15'),
        (hr_12, 10, 7, creative_version_dy-26, 'v' + str(creative_version_dy-26) + '_' + day_id + '15'),
        (hr_12, 10, 7, creative_version_dy-27, 'v' + str(creative_version_dy-27) + '_' + day_id + '15'),
        (hr_12, 10, 7, creative_version_dy-28, 'v' + str(creative_version_dy-28) + '_' + day_id + '14'),
        (hr_12, 10, 7, creative_version_dy-29, 'v' + str(creative_version_dy-29) + '_' + day_id + '14'),
        (hr_12, 10, 7, creative_version_dy-30, 'v' + str(creative_version_dy-30) + '_' + day_id + '14'),
        (hr_12, 10, 7, creative_version_dy-31, 'v' + str(creative_version_dy-31) + '_' + day_id + '13'),
        (hr_12, 10, 7, creative_version_dy-32, 'v' + str(creative_version_dy-32) + '_' + day_id + '13'),
        (hr_12, 10, 7, creative_version_dy-33, 'v' + str(creative_version_dy-33) + '_' + day_id + '13'),
        (hr_12, 10, 7, creative_version_dy-34, 'v' + str(creative_version_dy-34) + '_' + day_id + '12'),
        (hr_12, 10, 7, creative_version_dy-35, 'v' + str(creative_version_dy-35) + '_' + day_id + '12'),
        (hr_12, 10, 7, creative_version_dy-36, 'v' + str(creative_version_dy-36) + '_' + day_id + '12'),
        (hr_12, 10, 7, creative_version_dy-37, 'v' + str(creative_version_dy-37) + '_' + day_id + '11'),
        (hr_12, 10, 7, creative_version_dy-38, 'v' + str(creative_version_dy-38) + '_' + day_id + '11'),
        (hr_12, 10, 7, creative_version_dy-39, 'v' + str(creative_version_dy-39) + '_' + day_id + '11'),
        (hr_12, 10, 7, creative_version_dy-40, 'v' + str(creative_version_dy-40) + '_' + day_id + '10'),
        (hr_12, 10, 7, creative_version_dy-41, 'v' + str(creative_version_dy-41) + '_' + day_id + '10'),
        (hr_12, 10, 7, creative_version_dy-42, 'v' + str(creative_version_dy-42) + '_' + day_id + '10'),
        (hr_12, 10, 7, creative_version_dy-43, 'v' + str(creative_version_dy-43) + '_' + day_id + '09'),
        (hr_12, 10, 7, creative_version_dy-44, 'v' + str(creative_version_dy-44) + '_' + day_id + '09'),
        (hr_12, 10, 7, creative_version_dy-45, 'v' + str(creative_version_dy-45) + '_' + day_id + '09'),
        (hr_12, 10, 7, creative_version_dy-46, 'v' + str(creative_version_dy-46) + '_' + day_id + '08'),
        (hr_12, 10, 7, creative_version_dy-47, 'v' + str(creative_version_dy-47) + '_' + day_id + '08'),
        (hr_12, 10, 7, creative_version_dy-48, 'v' + str(creative_version_dy-48) + '_' + day_id + '08'),
        (hr_12, 10, 7, creative_version_dy-49, 'v' + str(creative_version_dy-49) + '_' + day_id + '07'),
        (hr_12, 10, 7, creative_version_dy-50, 'v' + str(creative_version_dy-50) + '_' + day_id + '07'),
        (hr_12, 10, 7, creative_version_dy-51, 'v' + str(creative_version_dy-51) + '_' + day_id + '07'),
        (hr_12, 10, 7, creative_version_dy-52, 'v' + str(creative_version_dy-52) + '_' + day_id + '06'),
        (hr_12, 10, 7, creative_version_dy-53, 'v' + str(creative_version_dy-53) + '_' + day_id + '06'),
        (hr_12, 10, 7, creative_version_dy-54, 'v' + str(creative_version_dy-54) + '_' + day_id + '06'),
        (hr_12, 10, 7, creative_version_dy-55, 'v' + str(creative_version_dy-55) + '_' + day_id + '05'),
        (hr_12, 10, 7, creative_version_dy-56, 'v' + str(creative_version_dy-56) + '_' + day_id + '05'),
        (hr_12, 10, 7, creative_version_dy-57, 'v' + str(creative_version_dy-57) + '_' + day_id + '05'),
        (hr_12, 10, 7, creative_version_dy-58, 'v' + str(creative_version_dy-58) + '_' + day_id + '04'),
        (hr_12, 10, 7, creative_version_dy-59, 'v' + str(creative_version_dy-59) + '_' + day_id + '04'),
        (hr_12, 10, 7, creative_version_dy-60, 'v' + str(creative_version_dy-60) + '_' + day_id + '04'),
        (hr_12, 10, 7, creative_version_dy-61, 'v' + str(creative_version_dy-61) + '_' + day_id + '03'),
        (hr_12, 10, 7, creative_version_dy-62, 'v' + str(creative_version_dy-62) + '_' + day_id + '03'),
        (hr_12, 10, 7, creative_version_dy-63, 'v' + str(creative_version_dy-63) + '_' + day_id + '03'),
        (hr_12, 10, 7, creative_version_dy-64, 'v' + str(creative_version_dy-64) + '_' + day_id + '02'),
        (hr_12, 10, 7, creative_version_dy-65, 'v' + str(creative_version_dy-65) + '_' + day_id + '02'),
        (hr_12, 10, 7, creative_version_dy-66, 'v' + str(creative_version_dy-66) + '_' + day_id + '02'),
        (hr_12, 10, 7, creative_version_dy-67, 'v' + str(creative_version_dy-67) + '_' + day_id + '01'),
        (hr_12, 10, 7, creative_version_dy-68, 'v' + str(creative_version_dy-68) + '_' + day_id + '01'),
        (hr_12, 10, 7, creative_version_dy-69, 'v' + str(creative_version_dy-69) + '_' + day_id + '01'),
        (hr_12, 10, 7, creative_version_dy-70, 'v' + str(creative_version_dy-70) + '_' + day_id + '00'),
        (hr_12, 10, 7, creative_version_dy-71, 'v' + str(creative_version_dy-71) + '_' + day_id + '00'),
        (hr_12, 10, 7, creative_version_dy-72, 'v' + str(creative_version_dy-72) + '_' + day_id + '00'),
        # ----------------------------------------------------------------------------------------------------
        # verification for EST microstrategy reports
        # ----------------------------------------------------------------------------------------------------
        (hr_07, 10, 7, creative_version_dy+15, 'v' + str(creative_version_dy+15) + '_' + next_day_id + '04'),
        (hr_07, 10, 7, creative_version_dy+14, 'v' + str(creative_version_dy+14) + '_' + next_day_id + '04'),
        (hr_07, 10, 7, creative_version_dy+13, 'v' + str(creative_version_dy+13) + '_' + next_day_id + '04'),
        (hr_07, 10, 7, creative_version_dy+12, 'v' + str(creative_version_dy+12) + '_' + next_day_id + '03'),
        (hr_07, 10, 7, creative_version_dy+11, 'v' + str(creative_version_dy+11) + '_' + next_day_id + '03'),
        (hr_07, 10, 7, creative_version_dy+10, 'v' + str(creative_version_dy+10) + '_' + next_day_id + '03'),
        (hr_07, 10, 7, creative_version_dy+9, 'v' + str(creative_version_dy+9) + '_' + next_day_id + '02'),
        (hr_07, 10, 7, creative_version_dy+8, 'v' + str(creative_version_dy+8) + '_' + next_day_id + '02'),
        (hr_07, 10, 7, creative_version_dy+7, 'v' + str(creative_version_dy+7) + '_' + next_day_id + '02'),
        (hr_07, 10, 7, creative_version_dy+6, 'v' + str(creative_version_dy+6) + '_' + next_day_id + '01'),
        (hr_07, 10, 7, creative_version_dy+5, 'v' + str(creative_version_dy+5) + '_' + next_day_id + '01'),
        (hr_07, 10, 7, creative_version_dy+4, 'v' + str(creative_version_dy+4) + '_' + next_day_id + '01'),
        (hr_07, 10, 7, creative_version_dy+3, 'v' + str(creative_version_dy+3) + '_' + next_day_id + '00'),
        (hr_07, 10, 7, creative_version_dy+2, 'v' + str(creative_version_dy+2) + '_' + next_day_id + '00'),
        (hr_07, 10, 7, creative_version_dy+1, 'v' + str(creative_version_dy+1) + '_' + next_day_id + '00'),   # hour 00
        # UTC day boundary
        #(hr_07, 2, 2, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),      # hour 23
        (hr_07, 3, 2, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),      # hour 23
        (hr_07, 10, 7, creative_version_dy-1, 'v' + str(creative_version_dy-1) + '_' + day_id + '23'),
        (hr_07, 10, 7, creative_version_dy-2, 'v' + str(creative_version_dy-2) + '_' + day_id + '23'),
        (hr_07, 10, 7, creative_version_dy-3, 'v' + str(creative_version_dy-3) + '_' + day_id + '23'),
        (hr_07, 10, 7, creative_version_dy-4, 'v' + str(creative_version_dy-4) + '_' + day_id + '22'),
        (hr_07, 10, 7, creative_version_dy-5, 'v' + str(creative_version_dy-5) + '_' + day_id + '22'),
        (hr_07, 10, 7, creative_version_dy-6, 'v' + str(creative_version_dy-6) + '_' + day_id + '22'),
        (hr_07, 10, 7, creative_version_dy-7, 'v' + str(creative_version_dy-7) + '_' + day_id + '21'),
        (hr_07, 10, 7, creative_version_dy-8, 'v' + str(creative_version_dy-8) + '_' + day_id + '21'),
        (hr_07, 10, 7, creative_version_dy-9, 'v' + str(creative_version_dy-9) + '_' + day_id + '21'),
        (hr_07, 10, 7, creative_version_dy-10, 'v' + str(creative_version_dy-10) + '_' + day_id + '20'),
        (hr_07, 10, 7, creative_version_dy-11, 'v' + str(creative_version_dy-11) + '_' + day_id + '20'),
        (hr_07, 10, 7, creative_version_dy-12, 'v' + str(creative_version_dy-12) + '_' + day_id + '20'),
        (hr_07, 10, 7, creative_version_dy-13, 'v' + str(creative_version_dy-13) + '_' + day_id + '19'),
        (hr_07, 10, 7, creative_version_dy-14, 'v' + str(creative_version_dy-14) + '_' + day_id + '19'),
        (hr_07, 10, 7, creative_version_dy-15, 'v' + str(creative_version_dy-15) + '_' + day_id + '19'),
        (hr_07, 10, 7, creative_version_dy-16, 'v' + str(creative_version_dy-16) + '_' + day_id + '18'),
        (hr_07, 10, 7, creative_version_dy-17, 'v' + str(creative_version_dy-17) + '_' + day_id + '18'),
        (hr_07, 10, 7, creative_version_dy-18, 'v' + str(creative_version_dy-18) + '_' + day_id + '18'),
        (hr_07, 10, 7, creative_version_dy-19, 'v' + str(creative_version_dy-19) + '_' + day_id + '17'),
        (hr_07, 10, 7, creative_version_dy-20, 'v' + str(creative_version_dy-20) + '_' + day_id + '17'),
        (hr_07, 10, 7, creative_version_dy-21, 'v' + str(creative_version_dy-21) + '_' + day_id + '17'),
        (hr_07, 10, 7, creative_version_dy-22, 'v' + str(creative_version_dy-22) + '_' + day_id + '16'),
        (hr_07, 10, 7, creative_version_dy-23, 'v' + str(creative_version_dy-23) + '_' + day_id + '16'),
        (hr_07, 10, 7, creative_version_dy-24, 'v' + str(creative_version_dy-24) + '_' + day_id + '16'),
        (hr_07, 10, 7, creative_version_dy-25, 'v' + str(creative_version_dy-25) + '_' + day_id + '15'),
        (hr_07, 10, 7, creative_version_dy-26, 'v' + str(creative_version_dy-26) + '_' + day_id + '15'),
        (hr_07, 10, 7, creative_version_dy-27, 'v' + str(creative_version_dy-27) + '_' + day_id + '15'),
        (hr_07, 10, 7, creative_version_dy-28, 'v' + str(creative_version_dy-28) + '_' + day_id + '14'),
        (hr_07, 10, 7, creative_version_dy-29, 'v' + str(creative_version_dy-29) + '_' + day_id + '14'),
        (hr_07, 10, 7, creative_version_dy-30, 'v' + str(creative_version_dy-30) + '_' + day_id + '14'),
        (hr_07, 10, 7, creative_version_dy-31, 'v' + str(creative_version_dy-31) + '_' + day_id + '13'),
        (hr_07, 10, 7, creative_version_dy-32, 'v' + str(creative_version_dy-32) + '_' + day_id + '13'),
        (hr_07, 10, 7, creative_version_dy-33, 'v' + str(creative_version_dy-33) + '_' + day_id + '13'),
        (hr_07, 10, 7, creative_version_dy-34, 'v' + str(creative_version_dy-34) + '_' + day_id + '12'),
        (hr_07, 10, 7, creative_version_dy-35, 'v' + str(creative_version_dy-35) + '_' + day_id + '12'),
        (hr_07, 10, 7, creative_version_dy-36, 'v' + str(creative_version_dy-36) + '_' + day_id + '12'),
        (hr_07, 10, 7, creative_version_dy-37, 'v' + str(creative_version_dy-37) + '_' + day_id + '11'),
        (hr_07, 10, 7, creative_version_dy-38, 'v' + str(creative_version_dy-38) + '_' + day_id + '11'),
        (hr_07, 10, 7, creative_version_dy-39, 'v' + str(creative_version_dy-39) + '_' + day_id + '11'),
        (hr_07, 10, 7, creative_version_dy-40, 'v' + str(creative_version_dy-40) + '_' + day_id + '10'),
        (hr_07, 10, 7, creative_version_dy-41, 'v' + str(creative_version_dy-41) + '_' + day_id + '10'),
        (hr_07, 10, 7, creative_version_dy-42, 'v' + str(creative_version_dy-42) + '_' + day_id + '10'),
        (hr_07, 10, 7, creative_version_dy-43, 'v' + str(creative_version_dy-43) + '_' + day_id + '09'),
        (hr_07, 10, 7, creative_version_dy-44, 'v' + str(creative_version_dy-44) + '_' + day_id + '09'),
        (hr_07, 10, 7, creative_version_dy-45, 'v' + str(creative_version_dy-45) + '_' + day_id + '09'),
        (hr_07, 10, 7, creative_version_dy-46, 'v' + str(creative_version_dy-46) + '_' + day_id + '08'),
        (hr_07, 10, 7, creative_version_dy-47, 'v' + str(creative_version_dy-47) + '_' + day_id + '08'),
        (hr_07, 10, 7, creative_version_dy-48, 'v' + str(creative_version_dy-48) + '_' + day_id + '08'),
        (hr_07, 10, 7, creative_version_dy-49, 'v' + str(creative_version_dy-49) + '_' + day_id + '07'),
        (hr_07, 10, 7, creative_version_dy-50, 'v' + str(creative_version_dy-50) + '_' + day_id + '07'),
        (hr_07, 10, 7, creative_version_dy-51, 'v' + str(creative_version_dy-51) + '_' + day_id + '07'),
        (hr_07, 10, 7, creative_version_dy-52, 'v' + str(creative_version_dy-52) + '_' + day_id + '06'),
        (hr_07, 10, 7, creative_version_dy-53, 'v' + str(creative_version_dy-53) + '_' + day_id + '06'),
        (hr_07, 10, 7, creative_version_dy-54, 'v' + str(creative_version_dy-54) + '_' + day_id + '06'),
        (hr_07, 10, 7, creative_version_dy-55, 'v' + str(creative_version_dy-55) + '_' + day_id + '05'),
        (hr_07, 10, 7, creative_version_dy-56, 'v' + str(creative_version_dy-56) + '_' + day_id + '05'),
        (hr_07, 10, 7, creative_version_dy-57, 'v' + str(creative_version_dy-57) + '_' + day_id + '05')
    ]
    actual = lib.get_edw_f_ci_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)
    return verify_result('biw_owner.f_ci_tran_dy_cid', actual, expected, False)

# ------------------------------------------------------------------------------
def f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid, creative_version_dy):
    day_id = f_day_timezone_id_dayid
    previous_day_id = day_id_minus_days(day_id, 1)
    next_day_id = day_id_plus_days(day_id, 1)
    hr_12 = int(str(day_id) + str('12'))
    hr_07 = int(str(day_id) + str('07'))
    # f.day_timezone_id, f.vt_tran_qty, f.vt_revenue_amt, f.ct_tran_qty, f.ct_revenue_amt, dcv.creative_version, dcv.alias
    expected = [
        # ----------------------------------------------------------------------------------------------------
        # verification for UTC microstrategy reports
        # ----------------------------------------------------------------------------------------------------
        (hr_12, 1, 10, 1, 10, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),
        (hr_12, 1, 10, 3, 30, creative_version_dy-1, 'v' + str(creative_version_dy-1) + '_' + day_id + '23'),
        (hr_12, 2, 20, 5, 50, creative_version_dy-2, 'v' + str(creative_version_dy-2) + '_' + day_id + '23'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-3, 'v' + str(creative_version_dy-3) + '_' + day_id + '23'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-4, 'v' + str(creative_version_dy-4) + '_' + day_id + '22'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-5, 'v' + str(creative_version_dy-5) + '_' + day_id + '22'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-6, 'v' + str(creative_version_dy-6) + '_' + day_id + '22'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-7, 'v' + str(creative_version_dy-7) + '_' + day_id + '21'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-8, 'v' + str(creative_version_dy-8) + '_' + day_id + '21'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-9, 'v' + str(creative_version_dy-9) + '_' + day_id + '21'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-10, 'v' + str(creative_version_dy-10) + '_' + day_id + '20'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-11, 'v' + str(creative_version_dy-11) + '_' + day_id + '20'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-12, 'v' + str(creative_version_dy-12) + '_' + day_id + '20'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-13, 'v' + str(creative_version_dy-13) + '_' + day_id + '19'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-14, 'v' + str(creative_version_dy-14) + '_' + day_id + '19'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-15, 'v' + str(creative_version_dy-15) + '_' + day_id + '19'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-16, 'v' + str(creative_version_dy-16) + '_' + day_id + '18'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-17, 'v' + str(creative_version_dy-17) + '_' + day_id + '18'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-18, 'v' + str(creative_version_dy-18) + '_' + day_id + '18'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-19, 'v' + str(creative_version_dy-19) + '_' + day_id + '17'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-20, 'v' + str(creative_version_dy-20) + '_' + day_id + '17'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-21, 'v' + str(creative_version_dy-21) + '_' + day_id + '17'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-22, 'v' + str(creative_version_dy-22) + '_' + day_id + '16'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-23, 'v' + str(creative_version_dy-23) + '_' + day_id + '16'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-24, 'v' + str(creative_version_dy-24) + '_' + day_id + '16'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-25, 'v' + str(creative_version_dy-25) + '_' + day_id + '15'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-26, 'v' + str(creative_version_dy-26) + '_' + day_id + '15'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-27, 'v' + str(creative_version_dy-27) + '_' + day_id + '15'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-28, 'v' + str(creative_version_dy-28) + '_' + day_id + '14'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-29, 'v' + str(creative_version_dy-29) + '_' + day_id + '14'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-30, 'v' + str(creative_version_dy-30) + '_' + day_id + '14'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-31, 'v' + str(creative_version_dy-31) + '_' + day_id + '13'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-32, 'v' + str(creative_version_dy-32) + '_' + day_id + '13'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-33, 'v' + str(creative_version_dy-33) + '_' + day_id + '13'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-34, 'v' + str(creative_version_dy-34) + '_' + day_id + '12'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-35, 'v' + str(creative_version_dy-35) + '_' + day_id + '12'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-36, 'v' + str(creative_version_dy-36) + '_' + day_id + '12'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-37, 'v' + str(creative_version_dy-37) + '_' + day_id + '11'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-38, 'v' + str(creative_version_dy-38) + '_' + day_id + '11'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-39, 'v' + str(creative_version_dy-39) + '_' + day_id + '11'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-40, 'v' + str(creative_version_dy-40) + '_' + day_id + '10'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-41, 'v' + str(creative_version_dy-41) + '_' + day_id + '10'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-42, 'v' + str(creative_version_dy-42) + '_' + day_id + '10'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-43, 'v' + str(creative_version_dy-43) + '_' + day_id + '09'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-44, 'v' + str(creative_version_dy-44) + '_' + day_id + '09'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-45, 'v' + str(creative_version_dy-45) + '_' + day_id + '09'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-46, 'v' + str(creative_version_dy-46) + '_' + day_id + '08'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-47, 'v' + str(creative_version_dy-47) + '_' + day_id + '08'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-48, 'v' + str(creative_version_dy-48) + '_' + day_id + '08'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-49, 'v' + str(creative_version_dy-49) + '_' + day_id + '07'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-50, 'v' + str(creative_version_dy-50) + '_' + day_id + '07'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-51, 'v' + str(creative_version_dy-51) + '_' + day_id + '07'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-52, 'v' + str(creative_version_dy-52) + '_' + day_id + '06'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-53, 'v' + str(creative_version_dy-53) + '_' + day_id + '06'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-54, 'v' + str(creative_version_dy-54) + '_' + day_id + '06'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-55, 'v' + str(creative_version_dy-55) + '_' + day_id + '05'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-56, 'v' + str(creative_version_dy-56) + '_' + day_id + '05'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-57, 'v' + str(creative_version_dy-57) + '_' + day_id + '05'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-58, 'v' + str(creative_version_dy-58) + '_' + day_id + '04'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-59, 'v' + str(creative_version_dy-59) + '_' + day_id + '04'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-60, 'v' + str(creative_version_dy-60) + '_' + day_id + '04'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-61, 'v' + str(creative_version_dy-61) + '_' + day_id + '03'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-62, 'v' + str(creative_version_dy-62) + '_' + day_id + '03'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-63, 'v' + str(creative_version_dy-63) + '_' + day_id + '03'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-64, 'v' + str(creative_version_dy-64) + '_' + day_id + '02'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-65, 'v' + str(creative_version_dy-65) + '_' + day_id + '02'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-66, 'v' + str(creative_version_dy-66) + '_' + day_id + '02'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-67, 'v' + str(creative_version_dy-67) + '_' + day_id + '01'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-68, 'v' + str(creative_version_dy-68) + '_' + day_id + '01'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-69, 'v' + str(creative_version_dy-69) + '_' + day_id + '01'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-70, 'v' + str(creative_version_dy-70) + '_' + day_id + '00'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-71, 'v' + str(creative_version_dy-71) + '_' + day_id + '00'),
        (hr_12, 3, 30, 6, 60, creative_version_dy-72, 'v' + str(creative_version_dy-72) + '_' + day_id + '00'),
        (hr_12, 2, 20, 3, 30, creative_version_dy-74, 'v' + str(creative_version_dy-74) + '_' + previous_day_id + '23'),
        (hr_12, 1, 10, 1, 10, creative_version_dy-75, 'v' + str(creative_version_dy-75) + '_' + previous_day_id + '23'),
        # ----------------------------------------------------------------------------------------------------
        # verification for EST microstrategy reports
        # ----------------------------------------------------------------------------------------------------
        (hr_07, 1, 10, 3, 30, creative_version_dy+15, 'v' + str(creative_version_dy+15) + '_' + next_day_id + '04'),
        (hr_07, 2, 20, 5, 50, creative_version_dy+14, 'v' + str(creative_version_dy+14) + '_' + next_day_id + '04'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+13, 'v' + str(creative_version_dy+13) + '_' + next_day_id + '04'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+12, 'v' + str(creative_version_dy+12) + '_' + next_day_id + '03'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+11, 'v' + str(creative_version_dy+11) + '_' + next_day_id + '03'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+10, 'v' + str(creative_version_dy+10) + '_' + next_day_id + '03'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+9, 'v' + str(creative_version_dy+9) + '_' + next_day_id + '02'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+8, 'v' + str(creative_version_dy+8) + '_' + next_day_id + '02'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+7, 'v' + str(creative_version_dy+7) + '_' + next_day_id + '02'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+6, 'v' + str(creative_version_dy+6) + '_' + next_day_id + '01'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+5, 'v' + str(creative_version_dy+5) + '_' + next_day_id + '01'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+4, 'v' + str(creative_version_dy+4) + '_' + next_day_id + '01'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+3, 'v' + str(creative_version_dy+3) + '_' + next_day_id + '00'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+2, 'v' + str(creative_version_dy+2) + '_' + next_day_id + '00'),
        (hr_07, 3, 30, 6, 60, creative_version_dy+1, 'v' + str(creative_version_dy+1) + '_' + next_day_id + '00'),  # hour 00
        # day boundary
        (hr_07, 1, 10, 1, 10, creative_version_dy, 'v' + str(creative_version_dy) + '_' + day_id + '23'),  # hour 23
        (hr_07, 3, 30, 6, 60, creative_version_dy-1, 'v' + str(creative_version_dy-1) + '_' + day_id + '23'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-2, 'v' + str(creative_version_dy-2) + '_' + day_id + '23'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-3, 'v' + str(creative_version_dy-3) + '_' + day_id + '23'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-4, 'v' + str(creative_version_dy-4) + '_' + day_id + '22'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-5, 'v' + str(creative_version_dy-5) + '_' + day_id + '22'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-6, 'v' + str(creative_version_dy-6) + '_' + day_id + '22'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-7, 'v' + str(creative_version_dy-7) + '_' + day_id + '21'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-8, 'v' + str(creative_version_dy-8) + '_' + day_id + '21'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-9, 'v' + str(creative_version_dy-9) + '_' + day_id + '21'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-10, 'v' + str(creative_version_dy-10) + '_' + day_id + '20'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-11, 'v' + str(creative_version_dy-11) + '_' + day_id + '20'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-12, 'v' + str(creative_version_dy-12) + '_' + day_id + '20'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-13, 'v' + str(creative_version_dy-13) + '_' + day_id + '19'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-14, 'v' + str(creative_version_dy-14) + '_' + day_id + '19'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-15, 'v' + str(creative_version_dy-15) + '_' + day_id + '19'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-16, 'v' + str(creative_version_dy-16) + '_' + day_id + '18'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-17, 'v' + str(creative_version_dy-17) + '_' + day_id + '18'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-18, 'v' + str(creative_version_dy-18) + '_' + day_id + '18'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-19, 'v' + str(creative_version_dy-19) + '_' + day_id + '17'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-20, 'v' + str(creative_version_dy-20) + '_' + day_id + '17'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-21, 'v' + str(creative_version_dy-21) + '_' + day_id + '17'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-22, 'v' + str(creative_version_dy-22) + '_' + day_id + '16'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-23, 'v' + str(creative_version_dy-23) + '_' + day_id + '16'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-24, 'v' + str(creative_version_dy-24) + '_' + day_id + '16'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-25, 'v' + str(creative_version_dy-25) + '_' + day_id + '15'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-26, 'v' + str(creative_version_dy-26) + '_' + day_id + '15'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-27, 'v' + str(creative_version_dy-27) + '_' + day_id + '15'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-28, 'v' + str(creative_version_dy-28) + '_' + day_id + '14'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-29, 'v' + str(creative_version_dy-29) + '_' + day_id + '14'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-30, 'v' + str(creative_version_dy-30) + '_' + day_id + '14'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-31, 'v' + str(creative_version_dy-31) + '_' + day_id + '13'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-32, 'v' + str(creative_version_dy-32) + '_' + day_id + '13'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-33, 'v' + str(creative_version_dy-33) + '_' + day_id + '13'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-34, 'v' + str(creative_version_dy-34) + '_' + day_id + '12'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-35, 'v' + str(creative_version_dy-35) + '_' + day_id + '12'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-36, 'v' + str(creative_version_dy-36) + '_' + day_id + '12'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-37, 'v' + str(creative_version_dy-37) + '_' + day_id + '11'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-38, 'v' + str(creative_version_dy-38) + '_' + day_id + '11'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-39, 'v' + str(creative_version_dy-39) + '_' + day_id + '11'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-40, 'v' + str(creative_version_dy-40) + '_' + day_id + '10'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-41, 'v' + str(creative_version_dy-41) + '_' + day_id + '10'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-42, 'v' + str(creative_version_dy-42) + '_' + day_id + '10'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-43, 'v' + str(creative_version_dy-43) + '_' + day_id + '09'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-44, 'v' + str(creative_version_dy-44) + '_' + day_id + '09'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-45, 'v' + str(creative_version_dy-45) + '_' + day_id + '09'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-46, 'v' + str(creative_version_dy-46) + '_' + day_id + '08'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-47, 'v' + str(creative_version_dy-47) + '_' + day_id + '08'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-48, 'v' + str(creative_version_dy-48) + '_' + day_id + '08'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-49, 'v' + str(creative_version_dy-49) + '_' + day_id + '07'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-50, 'v' + str(creative_version_dy-50) + '_' + day_id + '07'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-51, 'v' + str(creative_version_dy-51) + '_' + day_id + '07'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-52, 'v' + str(creative_version_dy-52) + '_' + day_id + '06'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-53, 'v' + str(creative_version_dy-53) + '_' + day_id + '06'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-54, 'v' + str(creative_version_dy-54) + '_' + day_id + '06'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-55, 'v' + str(creative_version_dy-55) + '_' + day_id + '05'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-56, 'v' + str(creative_version_dy-56) + '_' + day_id + '05'),
        (hr_07, 3, 30, 6, 60, creative_version_dy-57, 'v' + str(creative_version_dy-57) + '_' + day_id + '05'),
        (hr_07, 2, 20, 3, 30, creative_version_dy-58, 'v' + str(creative_version_dy-58) + '_' + day_id + '04'),
        (hr_07, 1, 10, 1, 10, creative_version_dy-59, 'v' + str(creative_version_dy-59) + '_' + day_id + '04')
    ]
    actual = lib.get_edw_f_ping_assoc_tran_dy_cid(d_creative_id, f_day_timezone_id_dayid)
    return verify_result('biw_owner.f_ping_assoc_tran_dy_cid', actual, expected, False)


# ------------------------------------------------------------------------------
def verify_result(test_description, actual_result, expected_result, log_results_on_pass=True):
    print('----------------------------------------')
    print('Verifying ' + test_description + ' ...')
    if actual_result != expected_result:
        print(' - result: FAIL')
        result_passed = False
    else:
        print(' - result: Pass')
        result_passed = True
    if log_results_on_pass or not result_passed:
        bashCommand = AT_PROJECT_DIR + os.sep + 'common' + os.sep + 'util' + os.sep + 'diff_to_terminal.sh -s "' + str(actual_result) + '" "' + str(expected_result) + '"'
        output = subprocess.check_output(bashCommand, shell=True).decode('ascii').strip()
        print(output)
    return result_passed

# ------------------------------------------------------------------------------
def hour_id_minus_hours(hour_id_in, hours):
    dt_in = datetime.datetime.strptime(hour_id_in, "%Y%m%d%H")
    hour_id_out = dt_in - datetime.timedelta(hours=hours)
    return hour_id_out.strftime('%Y%m%d%H')

# ------------------------------------------------------------------------------
def hour_id_plus_hours(hour_id_in, hours):
    dt_in = datetime.datetime.strptime(hour_id_in, "%Y%m%d%H")
    hour_id_out = dt_in + datetime.timedelta(hours=hours)
    return hour_id_out.strftime('%Y%m%d%H')

# ------------------------------------------------------------------------------
def day_id_minus_days(day_id_in, days):
    dt_in = datetime.datetime.strptime(day_id_in, "%Y%m%d")
    day_id_out = dt_in - datetime.timedelta(days=days)
    return day_id_out.strftime('%Y%m%d')

# ------------------------------------------------------------------------------
def day_id_plus_days(day_id_in, days):
    dt_in = datetime.datetime.strptime(day_id_in, "%Y%m%d")
    day_id_out = dt_in + datetime.timedelta(days=days)
    return day_id_out.strftime('%Y%m%d')

# ------------------------------------------------------------------------------
def get_tz_delta():
    offset = time.timezone if (time.localtime().tm_isdst == 0) else time.altzone
    return offset / 60 / 60 * -1


# ######################################################################################################################
#   UNIT TESTS
# ######################################################################################################################

do_unit_tests = False
#do_unit_tests = True

if do_unit_tests:
    # get_tz_delta()
    # exit()

    # day_id and hour to verify
    day_id = '20160810'
    hour = '14'

    # creative id to verify
    creative_id = '5982897'

    # # 5982897hour_id_minus_hours()
    # h_id = day_id + hour
    # print('********************')
    # print(h_id)
    # print(hour_id_minus_hours(h_id, 1))
    # print(hour_id_minus_hours(h_id, -1))
    # print(hour_id_plus_hours(h_id, 1))
    # print(hour_id_plus_hours(h_id, -1))
    # print('********************')
    # print(day_id)
    # print(day_id_minus_days(day_id, 1))
    # print(day_id_minus_days(day_id, -1))
    # print(day_id_plus_days(day_id, 1))
    # print(day_id_plus_days(day_id, -1))
    # exit()

    # Verify multiple days and hours
    # days = ['20160627', '20160625']
    # hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
    # for d in days:
    #     for h in hours:
    #         run_hourly_verification(creative_id, d, h)
    #     run_daily_verification(creative_id, d)

    # verify a specific hour and day

    print('########################################')
    print(datetime.datetime.now())
    print(run_hourly_verification(creative_id, day_id, hour))

    # while run_hourly_verification(creative_id, day_id, hour) != 0:
    #     print('Sleeping for a minute...')
    #     time.sleep(60)
    #     print('########################################')
    #     print(datetime.datetime.now())
    # print(datetime.datetime.now())
    # #run_daily_verification(creative_id, day_id)


# ------------------------------------------------------------------------------
