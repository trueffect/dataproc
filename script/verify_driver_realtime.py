# ------------------------------------------------------------------------------
#
# Verify HOURLY end-to-end data for creative versions
#
# NOTE: The HOURLY verification needs to run at the beginning the hour.
#       It verifies the hour_id 2 hours ago.
#
# ------------------------------------------------------------------------------

import sys
import lib

# agency and campaign to get counts for
agency_id = '5975435'
campaign_id = '5982878'

print('########################################')
print('=== Running REALTIME verification ===')
print('agency_id: ' + agency_id)
print('campaign_id: ' + campaign_id)

result = lib.verify_realtime_counts(agency_id, campaign_id)
print('result: ' + str(result))

# return the test result
sys.exit(int(result))

# ------------------------------------------------------------------------------
