# ------------------------------------------------------------------------------
#
# Verify realtime intraday metrics for an Agency and list of campaigns
#
# ------------------------------------------------------------------------------

import sys
import lib

# agency and list of campaigns to get counts for
# campaign_id = '6566237'
# placement_list = 6566248, 6566251

campaign_id = '5982878'
placement_list = [5982883]


print('########################################')
print('=== Running realtime INTRADAY verification for Placement ===')
print(' - campaign_id: ' + campaign_id)
print(' - placement_list: ' + str(placement_list))

result = lib.verify_realtime_counts_intraday_placements(campaign_id, placement_list)
print('result: ' + str(result))

# return the test result
sys.exit(int(result))

# ------------------------------------------------------------------------------
