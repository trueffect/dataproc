# ------------------------------------------------------------------------------
#
# Batch 3 for Associated and Unassociated Data, including CT and VT
#
# Spec: https://docs.google.com/spreadsheets/d/184hHlO7Thgnm2WwHyLL08osXYHX_7UUV2xNXHWWhg9s
#
# ------------------------------------------------------------------------------

import batch

# ------------------------------------------------------------------------------
def run_batch():
    # I, version, version, CP
    batch.play_tag('C', '22')
    batch.play_tag('P', '22')
    batch.play_tag('I', '24')

    # IC, version, version, P
    batch.play_tag('P', '25')
    batch.play_tag('I', '27')
    batch.play_tag('C', '27')

    # ----------------------------------------------------------
    # associated I,P transactions (VT)
    # ----------------------------------------------------------

    # IP same version
    batch.play_tag('I', '30')
    batch.play_tag('P', '30')

    # I, version, P
    batch.play_tag('P', '32')
    batch.play_tag('I', '33')

    # I, version, version, P
    batch.play_tag('P', '34')
    batch.play_tag('I', '36')

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

run_batch()
batch.simulate_rt()

# ------------------------------------------------------------------------------
