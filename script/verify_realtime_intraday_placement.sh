#!/usr/bin/env bash

# get current timestamp
timestamp=`date -u +%Y%m%d\(%H:%M\)`
echo "Current Time: $timestamp"

# success email
success_email="qa@trueffect.com"

# failure email
fail_email="qa@trueffect.com,tellebracht@trueffect.com,jrickard@trueffect.com"

# qa server's hostname
qaserver="mercury.trueffect.com"

# make the script portable so it runs on both mercury and the localhost
echo "Hostname: $HOSTNAME"
if [ $HOSTNAME = "$qaserver" ]; then
    echo "Hi $qaserver!"
    export ORACLE_HOME=/app/oracle/11g
    export LD_LIBRARY_PATH=/app/oracle/11g/lib
    pyexe="/usr/local/python/bin/python3"
    pypath="/home/prodman/simulateTraffic/end_to_end/dataproc/script/"
else
    echo "Not $qaserver"
    fail_email="wbutler@trueffect.com"
    pyexe="/usr/local/bin/python3"
    pypath=""
fi

# temporarily send emails to me until scripts are passing again
fail_email="wbutler@trueffect.com"
success_email="wbutler@trueffect.com"

# prepare and execute the python script
cmd=$pyexe" "$pypath"verify_driver_realtime_intraday_placement.py"
echo "cmd: $cmd"
output=`${cmd}`
ret=$?
echo "Result: {$output}"

# send email for result
echo $ret
if [ $ret -ne 0 ]; then
    #Fail
    echo "Sending FAIL email..."
    echo -e "Result: ${output}" | mail -s "QA Realtime Intraday Placement FAILED - $timestamp" "$fail_email"
else
    #Pass
    echo "Sending PASS email..."
    echo -e "Realtime is looking good :)  Result: ${output}" | mail -s "QA Realtime Intraday Placement PASSED - $timestamp" "$success_email"
fi
echo " - done sending email"
