#!/bin/bash

# This script counts the number of
#   I,C,P records in a data extract

# vars
EXPECTED_I_ROWS=30
EXPECTED_C_ROWS=21
EXPECTED_P_ROWS=30
EXIT_CODE=0

# email lists
PASS_EMAIL_LIST="qa@trueffect.com"
WARN_EMAIL_LIST="qa@trueffect.com"
FAIL_EMAIL_LIST="qa@trueffect.com,todd.ellebracht@trueffect.com"

# state files
LAST_HR_FILE="/home/prodman/simulateTraffic/end_to_end/dataproc/last_hour_processed.dat"
HOUR_FILE="/opt/trueffect/dataproc/extract/bin/progress-trans_extract.state"

# terminal colors
red=`tput setaf 1`
redbk=`tput setab 1`
green=`tput setaf 2`
greenbk=`tput setab 2`
yellow=`tput setaf 3`
yellowbk=`tput setab 3`
white=`tput setaf 7`
whitebk=`tput setab 7`
bold=`tput bold`
reset=`tput sgr0`

# display script name
echo "----------------------------------------"
echo "==> ${0##*/}"

# command args
if [ "$#" -eq 1 ]; then
	HOUR_ID=$1
elif [ "$#" -eq 0 ]; then
	HOUR_ID=$(head -n 1 $HOUR_FILE) #read first line of file
	if [ $? -ne 0 ]; then
	        echo "Unexpected error trying to read '$HOUR_FILE'"
	        exit 200
	fi
elif [ "$#" -gt 1 ]; then
    echo "Illegal number of parameters"
	echo "----------------------------------------"
	exit 100
fi

# get last hour that THIS script processed
LAST_HR=$(head -n 1 $LAST_HR_FILE) # read first line of file
if [ $? -ne 0 ]; then
        echo "Unexpected error trying to read '$LAST_HR_FILE'"
        exit 300
fi

echo "extract hour_id to process:   '$HOUR_ID'"
echo "hour last processed by me: '$LAST_HR'"

# there are extra I,C,P for hour 23 that need to be dealt with
HR=${HOUR_ID: -2}
echo "hour to process: '$HR'"
if [ $HR == '23' ]; then
echo " - need to add extra counts for hour 23"
EXPECTED_I_ROWS=33
EXPECTED_C_ROWS=23
EXPECTED_P_ROWS=33
echo "EXPECTED_I_ROWS: $EXPECTED_I_ROWS"
echo "EXPECTED_C_ROWS: $EXPECTED_C_ROWS"
echo "EXPECTED_P_ROWS: $EXPECTED_P_ROWS"
else
echo " - not HR 23"
echo "EXPECTED_I_ROWS: $EXPECTED_I_ROWS"
echo "EXPECTED_C_ROWS: $EXPECTED_C_ROWS"
echo "EXPECTED_P_ROWS: $EXPECTED_P_ROWS"
fi
# exit 99

DATA_EXTRACT_FILE="/mstg/user/prodman/ftp/e2etest/Outgoing/"$HOUR_ID"_tr003_trans_e2etest.dat.gz"
echo "$DATA_EXTRACT_FILE"

# if hours are the same, we already processed the hour
if [ $HOUR_ID == $LAST_HR ]; then
	mail -s "QA E2E Hourly Extract WARNING" $PASS_EMAIL_LIST <<< "Standard Hourly Extract for hour '$HOUR_ID' has already been verified.  Data appears to be lagging.."
	echo "${yellowbk}${white}${bold}Hour '$HOUR_ID' already processed!${reset}"
	exit 0
fi

# get counts
I_ROWS=`/bin/zcat $DATA_EXTRACT_FILE | /usr/bin/cut -f6 | /bin/grep I | wc -l`
C_ROWS=`/bin/zcat $DATA_EXTRACT_FILE | /usr/bin/cut -f6 | /bin/grep C | wc -l`
P_ROWS=`/bin/zcat $DATA_EXTRACT_FILE | /usr/bin/cut -f6 | /bin/grep P | wc -l`

# validate counts
if [ $I_ROWS -ne $EXPECTED_I_ROWS ]; then
  EXIT_CODE=$((EXIT_CODE+1))
  echo -e "\tI\t${red}$I_ROWS${reset}"
else
  echo -e "\tI\t${green}$I_ROWS${reset}"
fi
if [ $C_ROWS -ne $EXPECTED_C_ROWS ]; then
  EXIT_CODE=$((EXIT_CODE+2))
  echo -e "\tC\t${red}$C_ROWS${reset}"
else
  echo -e "\tC\t${green}$C_ROWS${reset}"
fi
if [ $P_ROWS -ne $EXPECTED_P_ROWS ]; then
  EXIT_CODE=$((EXIT_CODE+4))
  echo -e "\tP\t${red}$P_ROWS${reset}"
else
  echo -e "\tP\t${green}$P_ROWS${reset}"
fi

# write current hour to state file
sed -i '1s/.*/'$HOUR_ID'/' $LAST_HR_FILE # update first row of state file with new HOUR_ID
if [ $? -ne 0 ]; then
        echo "Unexpected error trying to update '$LAST_HR_FILE' with value '$HOUR_ID'"
        exit 400
fi

# all done
if [ $EXIT_CODE -ne 0 ]; then
  echo "${redbk}${white}${bold}Verification FAILED with code: $EXIT_CODE${reset}"
  echo -e "Standard Hourly Extraction for hour $HOUR_ID failed with code $EXIT_CODE (I=$I_ROWS,C=$C_ROWS,P=$P_ROWS)\n\n$DATA_EXTRACT_FILE\n\n`/bin/zcat $DATA_EXTRACT_FILE | sort -t: -k2,4`" | mail -s "QA E2E Hourly Extract FAILED" $FAIL_EMAIL_LIST
else
  echo "${greenbk}${white}${bold}Verification PASSED${reset}"
  mail -s "QA E2E Hourly Extract PASSED" $PASS_EMAIL_LIST <<< "Standard Hourly Extract successfully verified for hour $HOUR_ID"
fi
exit $EXIT_CODE
