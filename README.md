This is an automated end-to-end test for data processing.  It works by generating
cURL commands against the ad server (entry point).  It also validates that each
I,C,P shows up in fact tables for microstrategy, and that all the creative versions
are associated properly (exit point).

NOTE: This only runs in the QA environment since DEV is not a full 'end-to-end' stack.

To Install or Update after modifying the test framework:
As prodman on MERCURY, run the following command:

`/home/prodman/simulateTraffic/end_to_end/deploy.sh`

NOTE: the deploy.sh script is checked in to the /scripts folder.  This script needs to be copied to /home/prodman/simulateTraffic/end_to_end/ if it doesn't exist for some reason in the future.


Config & Setup:
The following entries are required for prodman's crontab file on MERCURY:

# End-to-End tests
# NOTE: These are staggered so that none of them run exactly 30 minutes
#       after any other one does, to avoid API token expiration timing issue
04 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_1.py 1> /dev/null 2>&1
13 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_2.py 1> /dev/null 2>&1
22 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_3.py 1> /dev/null 2>&1
31 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_4.py 1> /dev/null 2>&1
40 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_5a.py 1> /dev/null 2>&1
55 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_6.py 1> /dev/null 2>&1
56 17 * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/hourly_end_to_end_batch_7.py 1> /dev/null 2>&1

# End-to-End verifications
05 * * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /bin/bash /home/prodman/simulateTraffic/end_to_end/dataproc/script/verify_hourly.sh 1> /dev/null 2>&1
05 07 * * * export ORACLE_HOME=/app/oracle/11g; export LD_LIBRARY_PATH=/app/oracle/11g/lib; /bin/bash /home/prodman/simulateTraffic/end_to_end/dataproc/script/verify_daily.sh 1> /dev/null 2>&1
# End-to-End standard extract verification
05 * * * * /home/prodman/simulateTraffic/end_to_end/dataproc/verifyHourlyDataExtract.sh 1> /dev/null 2>&1

# Play Video (generates R records)
15 * * * * /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/simulate_video.py 1> /dev/null 2>&1
45 17 * * * /usr/local/python/bin/python3 /home/prodman/simulateTraffic/end_to_end/dataproc/script/simulate_video.py 1> /dev/null 2>&1
